from fastapi import FastAPI
from routers.topics import topic_router
from routers.replies import reply_router
from routers.users import users_router
from routers.votes import vote_router
from routers.categories import category_router
from routers.conversations import conversation_router
from routers.home import home_router

app = FastAPI()

app.include_router(users_router)
app.include_router(topic_router)
app.include_router(reply_router)
app.include_router(vote_router)
app.include_router(category_router)
app.include_router(conversation_router)
app.include_router(home_router)