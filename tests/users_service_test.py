import unittest
import test_database as td
from services import users_service
from unittest.mock import Mock, patch, call, MagicMock

from data.models import User, UserOut


class UsersService_Should(unittest.TestCase):

    def test_getByEmail_returnsNone_whenUserDoesNotExist(self):
        user = td.fake_user()
        def get_data_func(query, params): return []
        result = users_service.get_by_email(user.email, get_data_func)
        self.assertIsNone(result)

    def test_isEmailInUse_returnsTrue_whenEmailExists(self):
        email = 'test@test.com'
        def get_data_func(query, params): return [(email,)]
        result = users_service.is_email_in_use(email, get_data_func)
        self.assertTrue(result)

    def test_isEmailInUse_returnsFalse_whenEmailDoesNotExist(self):
        email = 'test@test.com'
        def get_data_func(query, params): return []
        result = users_service.is_email_in_use(email, get_data_func)
        self.assertTrue(result)

    def test_isUsernameInUse_returnsFalse_whenUsernameNotInUse(self):
        username = td.fake_user().username
        def get_data_func(query, params): return []
        result = users_service.is_username_in_use(username, get_data_func)
        self.assertTrue(result)

    def test_isUsernameInUse_returnsTrue_whenUsernameInUse(self):
        username = td.fake_user().username
        def get_data_func(query, params): return [(username,)]
        result = users_service.is_username_in_use(username, get_data_func)
        self.assertTrue(result)

    def test_getByEmail_returnsNone_whenUserDoesNotExist(self):
        # Arrange
        user = td.fake_user()

        def get_data_func(query, params):
            return []

        # Act
        result = users_service.get_by_email(user.email, get_data_func)

        # Assert
        self.assertIsNone(result)

    def test_get_stored_password_returns_password(self):
        user = td.fake_user()
        result = users_service.get_stored_password(user.email)
        self.assertIsNotNone(result)

    def test_create_returnsUnprocessable_withUsernameAndEmailAlreadyInUse(self):
        # Arrange
        user = td.fake_user()
        mock_is_email_in_use = MagicMock(return_value=True)
        mock_is_username_in_use = MagicMock(return_value=True)
        users_service.is_email_in_use = mock_is_email_in_use
        users_service.is_username_in_use = mock_is_username_in_use

        # Act
        result = users_service.create(user)

        # Assert
        self.assertEqual(result.status_code, 422)
    
    def test_create_returnsOk_withValidUser(self):
        # Arrange
        user = td.fake_user_password()
        mock_is_email_in_use = MagicMock(return_value=False)
        mock_is_username_in_use = MagicMock(return_value=False)
        users_service.is_email_in_use = mock_is_email_in_use
        users_service.is_username_in_use = mock_is_username_in_use

        # Act
        result = users_service.create(user)

        # Assert
        self.assertEqual(result, f"Your username:{user.username}, email:{user.email}. You can now login.")

    def test_getByEmail_returnsNone_whenUserEmailIsNone(self):
        # Arrange
        email = None

        # Act
        result = users_service.get_by_email(email, None)

        # Assert
        self.assertIsNone(result)

    def test_create_returnsUnprocessable_withEmailAlreadyInUse(self):
        # Arrange
        user = td.fake_user()
        mock_is_email_in_use = MagicMock(return_value=True)
        mock_is_username_in_use = MagicMock(return_value=False)
        users_service.is_email_in_use = mock_is_email_in_use
        users_service.is_username_in_use = mock_is_username_in_use

        # Act
        result = users_service.create(user)

        # Assert
        self.assertEqual(result.status_code, 422)
    
    def test_create_returnsUnprocessable_withUsernameAlreadyInUse(self):
        # Arrange
        user = td.fake_user()
        mock_is_email_in_use = MagicMock(return_value=False)
        mock_is_username_in_use = MagicMock(return_value=True)
        users_service.is_email_in_use = mock_is_email_in_use
        users_service.is_username_in_use = mock_is_username_in_use

        # Act
        result = users_service.create(user)

        # Assert
        self.assertEqual(result.status_code, 422)