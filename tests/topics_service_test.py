import unittest
import test_database as td
from services import topics_service
from unittest.mock import Mock, patch
from data.models import Topic, UserOut

class TopicsService_Should(unittest.TestCase):

    def test_getById_whenValid(self):
        expected_topic_id = 1
        expected_topic = td.fake_topic(id = expected_topic_id)
        get_data_func = lambda q, params: [
            (expected_topic.id, expected_topic.text, expected_topic.user_email, expected_topic.title, 
             expected_topic.category_name, expected_topic.is_locked)
        ]
        
        actual_topic = topics_service.get_topic_by_id(expected_topic_id, get_data_func)
        
        self.assertEqual(expected_topic, actual_topic)
        
    def test_getById_whenInvalid(self):
        
        invalid_topic_id = -1
        get_data_func = lambda query, params: []

        actual_topic = topics_service.get_topic_by_id(invalid_topic_id, get_data_func)

        self.assertIsNone(actual_topic)
    
    def test_exists_returnsTrue_when_topicExists(self):
        
        topic = td.fake_topic()
        get_data_func = lambda query, params: [(topic.id)]
        result = topics_service.topic_exists(topic.id, get_data_func)
        self.assertTrue(result)
    
    def test_exists_returnsFalse_when_topicDoesNotExists(self):
        
        topic = td.fake_topic()
        get_data_func = lambda query, params: []
        result = topics_service.topic_exists(topic.id, get_data_func)
        self.assertFalse(result)


    def test_all_whenNoSearch(self):
    
        topic = td.fake_topic()
        expected_user_email = topic.user_email
        expected_topics = [td.fake_topic(id=i) for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name, topic.is_locked)
            for topic in expected_topics
        ]
        current_user = td.fake_user(email=expected_user_email)

        actual_topics = topics_service.all(current_user, get_data_func=get_data_func)

        self.assertEqual(expected_topics, actual_topics)

    def test_all_whenSearch(self):
        
        topic = td.fake_topic()
        expected_user_email = topic.user_email
        search_string = 'Title 1'
        expected_topics = [td.fake_topic(id=i, title=f'Title {i}') for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name,  topic.is_locked)
            for topic in expected_topics if topic.title == 'Title 1'
        ]
        current_user = td.fake_user(email=expected_user_email)

        actual_topics = topics_service.all(current_user, search_string, get_data_func)
        self.assertEqual(expected_topics[0], actual_topics[0])

    def test_wanted_topics_whenSort(self):

        topic = td.fake_topic()
        expected_user_email = topic.user_email
        expected_topics = [td.fake_topic(id=i) for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name,  topic.is_locked)
            for topic in expected_topics
        ]
        current_user = td.fake_user(email=expected_user_email)

        actual_topics = topics_service.wanted_topics('asc', None, 'id', current_user, get_data_func)

        self.assertEqual(sorted(expected_topics, key=lambda t: t.id), actual_topics)

    def test_wanted_topics_whenNoSort(self):

        topic = td.fake_topic()
        expected_user_email = topic.user_email
        expected_topics = [td.fake_topic(id=i) for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name,  topic.is_locked)
            for topic in expected_topics
        ]
        current_user = td.fake_user(email=expected_user_email)

        actual_topics = topics_service.wanted_topics(None, None, None, current_user, get_data_func)

        self.assertEqual(expected_topics, actual_topics)

    def test_wanted_topics_whenDescSort(self):
    
        topic = td.fake_topic()
        expected_user_email = topic.user_email
        expected_topics = [td.fake_topic(id=i) for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name,  topic.is_locked)
            for topic in expected_topics
        ]
        current_user = td.fake_user(email=expected_user_email)
                                    
        actual_topics = topics_service.wanted_topics('desc', None, 'id', current_user, get_data_func)


        self.assertEqual(list(reversed(expected_topics)), actual_topics)
    
    def test_wanted_topics_whenSortByCategoryName(self):
        topic = td.fake_topic()
        expected_user_email = topic.user_email
        expected_topics = [td.fake_topic(id=i, category_name=f'Category {i}') for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name,  topic.is_locked)
            for topic in expected_topics
        ]
        current_user = td.fake_user(email=expected_user_email)
        expected_topics.sort(key=lambda t: t.category_name)

        actual_topics = topics_service.wanted_topics('asc', None, 'category_name', current_user, get_data_func)

        self.assertEqual(expected_topics, actual_topics)
    
    def test_wanted_topics_whenSortByTitle(self):
        topic = td.fake_topic()
        expected_user_email = topic.user_email
        expected_topics = [td.fake_topic(id=i, title=f'Title {i}') for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name,  topic.is_locked)
            for topic in expected_topics
        ]
        current_user = td.fake_user(email=expected_user_email)

        expected_topics.sort(key=lambda t: t.category_name)

        actual_topics = topics_service.wanted_topics('asc', None, 'title', current_user, get_data_func)

        self.assertEqual(expected_topics, actual_topics)

    def test_wanted_topics_whenSearch(self):
    
        topic = td.fake_topic()
        expected_user_email = topic.user_email
        expected_topics = [td.fake_topic(id=i, title=f'Title {i}') for i in range(1, 4)]
        get_data_func = lambda q, params: [
            (topic.id, topic.text, topic.user_email, topic.title, topic.category_name, topic.is_locked)
            for topic in expected_topics if topic.title == 'Title 2'
        ]
        current_user = td.fake_user(email=expected_user_email)

        actual_topics = topics_service.wanted_topics(None, 'Title 2', None, current_user, get_data_func)

        self.assertEqual([expected_topics[1]], actual_topics)

    def test_sort_topics_byTitleAsc(self):
        
        topics = [td.fake_topic(id=i, title=f'Title {i}') for i in range(3, 0, -1)]
        expected_order = sorted(topics, key=lambda topic: topic.title)

        actual_order = topics_service.sort_topics(topics, attribute='title')

        self.assertEqual(expected_order, actual_order)

    def test_sort_topics_byTitleDesc(self):

        topics = [td.fake_topic(id=i, title=f'Title {i}') for i in range(1, 4)]
        expected_order = sorted(topics, key=lambda topic: topic.title, reverse=True)

        actual_order = topics_service.sort_topics(topics, attribute='title', reverse=True)

        self.assertEqual(expected_order, actual_order)

    def test_sort_topics_byCategoryNameAsc(self):

        topics = [td.fake_topic(id=i, category_name=f'Category {i}') for i in range(3, 0, -1)]
        expected_order = sorted(topics, key=lambda topic: topic.category_name)

        actual_order = topics_service.sort_topics(topics, attribute='category_name')

        self.assertEqual(expected_order, actual_order)

    def test_sort_topics_byCategoryNameDesc(self):

        topics = [td.fake_topic(id=i, category_name=f'Category {i}') for i in range(1, 4)]
        expected_order = sorted(topics, key=lambda topic: topic.category_name, reverse=True)

        actual_order = topics_service.sort_topics(topics, attribute='category_name', reverse=True)

        self.assertEqual(expected_order, actual_order)

    def test_sort_topics_byIdAsc(self):

        topics = [td.fake_topic(id=i) for i in range(3, 0, -1)]
        expected_order = sorted(topics, key=lambda topic: topic.id)

        actual_order = topics_service.sort_topics(topics, attribute='id')

        self.assertEqual(expected_order, actual_order)

    def test_sort_topics_byIdDesc(self):

        topics = [td.fake_topic(id=i) for i in range(1, 4)]
        expected_order = sorted(topics, key=lambda topic: topic.id, reverse=True)

        actual_order = topics_service.sort_topics(topics,attribute='id', reverse=True)

        self.assertEqual(expected_order, actual_order)


    def test_create(self):

        topic = td.fake_topic(id=None, user_email=None)
        current_user = td.fake_user()
        expected_id = 123

        expected_topic = Topic(
            id=expected_id,
            text=topic.text,
            user_email=current_user.email,
            category_name=topic.category_name,
            title=topic.title,
            is_locked=topic.is_locked,
        )
        insert_data_func = lambda q, params: expected_id

        actual_topic = topics_service.create(topic, current_user, insert_data_func)

        self.assertEqual(expected_topic, actual_topic)
    
    def test_lock_whenTopicUnlocked(self):
        
        topic = td.fake_topic()
        expected_lock_status = 'locked'
        update_data_func = lambda q, params: None

        actual_topic = topics_service.lock(topic, expected_lock_status, update_data_func)

        self.assertEqual(expected_lock_status, actual_topic.is_locked)
    
    def test_lock_whenTopiclocked(self):
        
        topic = td.fake_topic(is_locked='locked')
        expected_lock_status = 'unlocked'
        update_data_func = lambda q, params: None

        actual_topic = topics_service.lock(topic, expected_lock_status, update_data_func)

        self.assertEqual(expected_lock_status, actual_topic.is_locked)

    def test_topic_is_locked_whenTrue(self):

        topic_id = 1
        get_data_func = lambda q, parameters: [1]

        actual_result = topics_service.topic_is_locked(topic_id, get_data_func)

        self.assertTrue(actual_result)

    def test_topic_is_locked_whenFalse(self):
        
        topic_id = 1
        get_data_func = lambda q, parameters: []

        actual_result = topics_service.topic_is_locked(topic_id, get_data_func)

        self.assertFalse(actual_result)




