from unittest.mock import Mock, MagicMock
from data.models import UserOut, Category, CategoryResponseModel, Pagination, UserAccessForCategory, Topic, ReplyResponseModel, BestReply, Message, MessageData
from common.responses import Forbidden
import random
import uuid
import string
import datetime

def fake_user(username='Test_user_customer', email='admin@abv.bg', role='admin'):
   mock_user = MagicMock(spec=UserOut)
   mock_user.username = username
   mock_user.role = role
   mock_user.email = email
   mock_user.is_admin = lambda: True
   return mock_user

def fake_category(name='Example category', is_private ='public', is_locked='unlocked'):
    mock_category = MagicMock(spec=Category)
    mock_category.name = name
    mock_category.is_private = is_private
    mock_category.is_locked = is_locked
    return mock_category

def fake_pagination(page=1, page_size=5, total_count=10, total_pages=2):
    mock_pagination = MagicMock(spec=Pagination)
    mock_pagination.page = page
    mock_pagination.page_size = page_size
    mock_pagination.total_count = total_count
    mock_pagination.total_pages = total_pages
    return mock_pagination

def fake_categoryResponseModel(categories=[(fake_category(name='cat 1')), (fake_category(name='cat 2'))],
                               pagination=fake_pagination()):
    
    mock_categoryResponseModel = MagicMock(spec=(list[Category], Pagination))
    mock_categoryResponseModel.categories=categories
    mock_categoryResponseModel.pagination=pagination
    return mock_categoryResponseModel

def fake_userAccessForCategory(email='admin@abv.bg', category='cat 1', access = 'full'):
    mock_userAccessForCategory = MagicMock(spec=UserAccessForCategory)
    mock_userAccessForCategory.email = email
    mock_userAccessForCategory.category = category
    mock_userAccessForCategory.access = access
    return mock_userAccessForCategory

def fake_response(exact_response, content, status_code=0 ):
    mock_response = MagicMock(spec=exact_response)
    mock_response.status_code = status_code
    mock_response.content = content
    return mock_response

def fake_topic(id = 1, text = 'Hola', user_email = 'stani@abv.bg', category_name = 'Databases', title = 'Saludo', is_locked = 'unlocked'):
    mock_topic = MagicMock(spec=Topic)
    mock_topic.id = id
    mock_topic.text = text
    mock_topic.user_email = user_email
    mock_topic.category_name = category_name
    mock_topic.title = title
    mock_topic.is_locked = is_locked
    return mock_topic

def fake_user_password(username='Test_user_customer', email='adminasdsad@abv.bg', password="asdafafsasf"):
    unique_suffix = ''.join(random.choices(string.ascii_lowercase, k=6))
    mock_user = MagicMock()
    mock_user.username = username + '_' + unique_suffix
    mock_user.email = f'{uuid.uuid4()}@example.com'
    mock_user.password = password
    mock_user.role = "admin"
    return mock_user

def fake_user_password_not_dynamic(username='Test_user_customer', email='adminasdsad@', password="asdafafsasf"):
    unique_suffix = ''.join(random.choices(string.ascii_lowercase, k=6))
    mock_user = MagicMock()
    mock_user.username = username + '_' + unique_suffix
    mock_user.email = email
    mock_user.password = password
    mock_user.role = "admin"
    return mock_user

def fake_reply(id=1, text='Example', topic_id=1,
               user_email='admin@abv.bg', best_reply=False, upvotes=2, downvotes=1):
    mock_reply = MagicMock(spec=ReplyResponseModel)
    mock_reply.id = id
    mock_reply.text = text
    mock_reply.topic_id = topic_id
    mock_reply.user_email = user_email
    mock_reply.best_reply = best_reply
    mock_reply.upvotes = upvotes
    mock_reply.downvotes = downvotes
    return mock_reply

def fake_bestReplyModel(best_reply=True):
    mock_bestReplyModel = MagicMock(spec=BestReply)
    mock_bestReplyModel.best_reply=best_reply
    return mock_bestReplyModel

def fake_user_access_for_category(email='admin@abv.bg', category='existing category', access='full'):
    mock_user_access_for_category = MagicMock(spec=UserAccessForCategory)
    mock_user_access_for_category.email = email
    mock_user_access_for_category.category = category
    mock_user_access_for_category.access=access
    return mock_user_access_for_category

def fake_message(id=1, text='Hola', sender='stani@gmail.com', receiver='vankata@gmail.com', creation_time = datetime.datetime.now()):
    mock_message = MagicMock(spec=Message)
    mock_message.id = id
    mock_message.text = text
    mock_message.sender = sender
    mock_message.receiver = receiver
    mock_message.creation_time = creation_time
    return mock_message

def fake_messageData(receiver_email='vankata@gmail.com', text='Hola', id=1):
    mock_messageData = MagicMock(spec=MessageData)
    mock_messageData.receiver_email = receiver_email
    mock_messageData.text = text
    mock_messageData.id = id
    return mock_messageData
   

    