import unittest
import test_database as td
from services import replies_service
from unittest.mock import patch, call


class RepliesService_Should(unittest.TestCase):

    def test_exists_returnsTrue_when_replyExists(self):
        get_data_func = lambda q, params: [(1,)]
        result = replies_service.exists(1, get_data_func)
        expected = True
        self.assertEqual(expected, result)


    def test_exists_returnsFalse_when_replyDoesNotExists(self):
        get_data_func = lambda q, params: []
        result = replies_service.exists(1, get_data_func)
        expected = False
        self.assertEqual(expected, result)

    def test_getByTopicId_returnsReply_when_replyExist(self):
        r = td.fake_reply()
        get_data_func = lambda q, params: [(1, 'Example', 1,'admin@abv.bg', False)]
        expected = r

        
        with patch('services.replies_service.ReplyResponseModel.from_query_result') as mock_from_query:
            mock_from_query.return_value = r

            with patch('services.replies_service.count_votes') as mock_count_votes:
                mock_count_votes.return_value = 2, 1
                actual_reply = replies_service.get_by_topic_id(1, get_data_func=get_data_func)
        
        for actual_reply, expected in zip(actual_reply, expected):
            self.assertEqual(actual_reply.id, expected.id)
            self.assertEqual(actual_reply.is_text, expected.text)
            self.assertEqual(actual_reply.user_email, expected.user_email)
            self.assertEqual(actual_reply.best_reply, expected.best_reply)
            self.assertEqual(actual_reply.upvotes, expected.upvotes)
            self.assertEqual(actual_reply.downvotes, expected.downvotes)

        mock_from_query.assert_called_once()
        mock_count_votes.assert_called_once()

    def test_getByTopicId_returnsNone_when_replyDoesNotExist(self):
        get_data_func = lambda q, params: []     
        actual_reply = replies_service.get_by_topic_id(1, get_data_func=get_data_func)
        self.assertIsNone(actual_reply)

    def test_getById_returnsReply_when_replyExist(self):
        r = td.fake_reply()
        get_data_func = lambda q, params: [(1, 'Example', 1,'admin@abv.bg', False)]
        expected = r
        
        with patch('services.replies_service.ReplyResponseModel.from_query_result') as mock_from_query:
            mock_from_query.return_value = r

            with patch('services.replies_service.count_votes') as mock_count_votes:
                mock_count_votes.return_value = 2, 1
                
                actual_reply = replies_service.get_by_id(1, get_data_func=get_data_func)
        
        for actual_reply, expected in zip(actual_reply, expected):
            self.assertEqual(actual_reply.id, expected.id)
            self.assertEqual(actual_reply.is_text, expected.text)
            self.assertEqual(actual_reply.user_email, expected.user_email)
            self.assertEqual(actual_reply.best_reply, expected.best_reply)
            self.assertEqual(actual_reply.upvotes, expected.upvotes)
            self.assertEqual(actual_reply.downvotes, expected.downvotes)

        mock_from_query.assert_called_once()
        mock_count_votes.assert_called_once()
        mock_count_votes.reset_mock()

    def test_getById_returnsNone_when_replyDoesNotExist(self):
        get_data_func = lambda q, params: []     
        actual_reply = replies_service.get_by_id(1, get_data_func=get_data_func)
        self.assertIsNone(actual_reply)

    def test_all_returnsReplyResponseModel(self):
        user = td.fake_user()
        reply1 = td.fake_reply()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        get_data_func = lambda query, params: [(1, 'Example', 1, 'admin@abv.bg', 0), (2, 'Two', 2, 'user@abv.bg', 1)]
        expected_response = [reply1, reply2]

        result = replies_service.all(current_logged_user=user, get_data_func=get_data_func)
        
        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)
    
    def test_all_returnsReplyResponseModel_with_emailFiltering(self):
        user = td.fake_user()
        reply1 = td.fake_reply()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        get_data_func = lambda query, params: [(2, 'Two', 2, 'user@abv.bg', 1)]
        expected_response = [reply2] 

        result = replies_service.all(current_logged_user=user, email_for_filtering='user@abv.bg', get_data_func=get_data_func)

        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)

    def test_all_returnsReplyResponseModel_with_search(self):
        user = td.fake_user()
        reply1 = td.fake_reply()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        get_data_func = lambda query, params: [(2, 'Two', 2, 'user@abv.bg', 1)]
        expected_response = [reply2] 

        result = replies_service.all(current_logged_user=user, search='Two', get_data_func=get_data_func)

        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)

    def test_all_returnsReplyResponseModel_with_emailFiltering_and_search(self):
        user = td.fake_user()
        reply1 = td.fake_reply()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        get_data_func = lambda query, params: [(2, 'Two', 2, 'user@abv.bg', 1)]
        expected_response = [reply2] 

        result = replies_service.all(current_logged_user=user, email_for_filtering='user@abv.bg', search='Two', get_data_func=get_data_func)

        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)

    def test_chooseBestReply_updatesBestReplyFlag_and_returnsUpdatedReply(self):
        reply = td.fake_reply()
        update_data_func = lambda q, p: [(1)]
        updated_reply = td.fake_reply(id=reply.id, best_reply=True)

        with patch('services.replies_service.get_by_id') as mock_get_by_id:
            mock_get_by_id.return_value = updated_reply

            result = replies_service.choose_best_reply(reply, best_reply_flag=td.fake_bestReplyModel(), update_data_func=update_data_func)

        self.assertEqual(result.id, updated_reply.id)
        self.assertEqual(result.text, updated_reply.text)
        self.assertEqual(result.user_email, updated_reply.user_email)
        self.assertEqual(result.best_reply, updated_reply.best_reply)

        mock_get_by_id.assert_called_once_with(reply.id)
        mock_get_by_id.reset_mock()


    def test_choose_best_reply_withDefaultUpdateDataFunc(self):

        reply = td.fake_reply()
        best_reply_flag = td.fake_bestReplyModel()
        updated_reply = td.fake_reply(id=reply.id, best_reply=True)

        def fake_update_query(query, params):
            nonlocal reply, updated_reply
            if query == 'UPDATE replies SET best_reply = 0 WHERE topic_id = ?':
                reply.best_reply = False
            elif query == 'UPDATE replies SET best_reply = ? WHERE id = ?':
                reply.best_reply = best_reply_flag.best_reply
            return None

        with patch('services.replies_service.get_by_id') as mock_get_by_id:
            mock_get_by_id.return_value = updated_reply

            with patch('services.replies_service.update_query') as mock_update_query:
                mock_update_query.side_effect = fake_update_query

                result = replies_service.choose_best_reply(reply, best_reply_flag)

        self.assertEqual(result.id, updated_reply.id)
        self.assertEqual(result.text, updated_reply.text)
        self.assertEqual(result.user_email, updated_reply.user_email)
        self.assertEqual(result.best_reply, updated_reply.best_reply)

        mock_get_by_id.assert_called_once_with(reply.id)
        mock_update_query.assert_has_calls([
            call('UPDATE replies SET best_reply = 0 WHERE topic_id = ?', (reply.topic_id,)),
            call('UPDATE replies SET best_reply = ? WHERE id = ?', (best_reply_flag.best_reply, reply.id))
        ])
        mock_get_by_id.reset_mock()
        mock_update_query.reset_mock()


    def test_create_insertsData_and_returnsCreatedReply(self):
        reply = td.fake_reply()
        current_user = td.fake_user()
        created_reply = td.fake_reply(id=1, text=reply.text, topic_id=reply.topic_id, user_email=current_user.email)

        def fake_insert_query(query, params):
            nonlocal reply, created_reply
            if query == 'INSERT INTO replies(text,topic_id,user_email) VALUES (?,?,?)':
                reply.id = created_reply.id
            return created_reply.id

        with patch('services.replies_service.get_by_id') as mock_get_by_id:
            mock_get_by_id.return_value = created_reply

            with patch('services.replies_service.insert_query') as mock_insert_query:
                mock_insert_query.side_effect = fake_insert_query

                result = replies_service.create(reply, current_user, insert_data_func=mock_insert_query)

        self.assertEqual(result.id, created_reply.id)
        self.assertEqual(result.text, created_reply.text)
        self.assertEqual(result.user_email, created_reply.user_email)
        self.assertEqual(result.best_reply, created_reply.best_reply)

        mock_get_by_id.assert_called_once_with(created_reply.id)
        mock_insert_query.assert_called_once_with(
            'INSERT INTO replies(text,topic_id,user_email) VALUES (?,?,?)',
            (reply.text, reply.topic_id, current_user.email)
        )
        mock_get_by_id.reset_mock()
        mock_insert_query.reset_mock()


    def test_delete_deletesReply_andVotes_and_returnsTrue(self):
        reply_id = 1

        def fake_update_query(query, params):
            nonlocal reply_id
            if query == 'DELETE FROM votes WHERE reply_id = ?':
                return True
            elif query == 'DELETE FROM replies WHERE id = ?':
                return True

        with patch('services.replies_service.update_query') as mock_update_query:
            mock_update_query.side_effect = fake_update_query(query = reply_id, params= None)

            result = replies_service.delete(reply_id, update_data_func=mock_update_query)

        self.assertTrue(result)
 
        mock_update_query.assert_has_calls([
            call('DELETE FROM votes WHERE reply_id = ?', (reply_id,)),
            call('DELETE FROM replies WHERE id = ?', (reply_id,))
        ])
        mock_update_query.reset_mock()        