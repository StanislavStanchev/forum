import unittest
import test_database as td
from routers import categories
from unittest.mock import patch, MagicMock
from data.models import Category, Pagination, Topic, UserAccessForCategory
from common.responses import Forbidden, NotFound, Conflict, BadRequest

def fake_topicsResponseModel(topics = [td.fake_topic(), td.fake_topic(id=2,text='Aloha')],pagination=td.fake_pagination()):
    mock_topicsResponseModel = MagicMock(spec=(list[Topic], Pagination ))
    mock_topicsResponseModel.topics =  topics
    mock_topicsResponseModel.pagination=pagination
    return mock_topicsResponseModel

class CategoriesRouter_Should(unittest.TestCase):
    
    def test_getCategories_returnsListOfCategories(self):
        current_user = td.fake_user()
        category1 = td.fake_category(name='cat 1')
        category2 = td.fake_category(name='cat 2')
        pagination = td.fake_pagination()
        expected_response = td.fake_categoryResponseModel(categories=[category1, category2], pagination=pagination)
                
        with patch('services.categories_service.all') as mock_categories:
            mock_categories.return_value = expected_response
    
            result = categories.get_categories(current_user=current_user)

        self.assertEqual(len(result.categories), len(expected_response.categories))
        self.assertIsInstance(result.pagination, Pagination)
        for actual_category, expected_category in zip(result.categories, expected_response.categories):
            self.assertIsInstance(actual_category, Category)

            self.assertEqual(actual_category.name, expected_category.name)
            self.assertEqual(actual_category.is_locked, expected_category.is_locked)
            self.assertEqual(actual_category.is_private, expected_category.is_private)

        self.assertEqual(result.pagination, expected_response.pagination)
        mock_categories.assert_called_once()
        
    def test_getCategories_returnsListOfCategories_when_search(self):
        current_user = td.fake_user()
        category2 = td.fake_category(name='cat 2')
        pagination = td.fake_pagination()
        expected_response = td.fake_categoryResponseModel(categories=[category2], pagination=pagination)
                
        with patch('services.categories_service.all') as mock_categories:
            mock_categories.return_value = expected_response
    
            result = categories.get_categories(current_user=current_user, search='2')

        self.assertEqual(len(result.categories), len(expected_response.categories))
        self.assertIsInstance(result.pagination, Pagination)
        for actual_category, expected_category in zip(result.categories, expected_response.categories):
            self.assertIsInstance(actual_category, Category)

            self.assertEqual(actual_category.name, expected_category.name)
            self.assertEqual(actual_category.is_locked, expected_category.is_locked)
            self.assertEqual(actual_category.is_private, expected_category.is_private)

        self.assertEqual(result.pagination, expected_response.pagination)
        mock_categories.assert_called_once()
    
    def test_getCategories_returnsListOfCategoriesInDescending_when_sortDesc(self):
        current_user = td.fake_user()
        category1 = td.fake_category(name='cat 1')
        category2 = td.fake_category(name='cat 2')
        pagination = td.fake_pagination()
        expected_response = td.fake_categoryResponseModel(categories=[category2, category1], pagination=pagination)
                
        with patch('services.categories_service.all') as mock_categories:
            mock_categories.return_value = expected_response
    
            result = categories.get_categories(current_user=current_user, sort='desc')

        self.assertEqual(len(result.categories), len(expected_response.categories))
        self.assertIsInstance(result.pagination, Pagination)
        for actual_category, expected_category in zip(result.categories, expected_response.categories):
            self.assertIsInstance(actual_category, Category)

            self.assertEqual(actual_category.name, expected_category.name)
            self.assertEqual(actual_category.is_locked, expected_category.is_locked)
            self.assertEqual(actual_category.is_private, expected_category.is_private)

        self.assertEqual(result.pagination, expected_response.pagination)
        mock_categories.assert_called_once()
    
    def test_getCategories_returnsBadRequest_when_InvalidParameters(self):

        current_user = td.fake_user()

        with patch('services.categories_service.all') as mock_categories:
            mock_categories.return_value = td.fake_response(exact_response=BadRequest,status_code=400, content="Search parameter must not be empty.")

            response_search = categories.get_categories(current_user=current_user, search=' ')
            response_sort = categories.get_categories(current_user=current_user, sort ='invalid')
            response_page = categories.get_categories(current_user=current_user, page = 0)
            response_page_size = categories.get_categories(current_user=current_user, page_size = 0)

        self.assertEqual(response_search.status_code, 400)
        self.assertEqual(response_sort.status_code, 400)
        self.assertEqual(response_page.status_code, 400)
        self.assertEqual(response_page_size.status_code, 400)

        self.assertIsInstance(response_search, BadRequest)
        self.assertIsInstance(response_sort, BadRequest)
        self.assertIsInstance(response_page, BadRequest)
        self.assertIsInstance(response_page_size, BadRequest)
    
    def test_getCategory_returnsNotFound_when_CategoryDoesNotExist(self):
        name = "non-existing-category"
        current_user = td.fake_user()
        expected_response = td.fake_response(exact_response=NotFound,status_code=404, content='')

        with patch('services.categories_service.get_by_name') as mock_get_by_name:
            mock_get_by_name.return_value = None
    
            result = categories.get_category(name=name, current_user=current_user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, NotFound)
        mock_get_by_name.assert_called_once()
    

    def test_getCategory_returnsUnauthorized_when_notMember(self):
        category = td.fake_category()
        current_user = td.fake_user()
        expected_response = td.fake_response(exact_response=Forbidden,status_code=403,
                                              content=Forbidden(content=f'Restricted access to category "{category.name}"'))

        with patch('services.categories_service.get_by_name') as mock_get_by_name:
            mock_get_by_name.return_value = category

            with patch('services.categories_service.validate_category_access') as mock_validation:
                mock_validation.return_value = expected_response
    
                result = categories.get_category(name=category.name, current_user=current_user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertEqual(result.content, expected_response.content)
        self.assertIsInstance(result, Forbidden)
        mock_get_by_name.assert_called_once()
        mock_validation.assert_called_once()


    def test_getCategory_returnsCategoryAndPaginatedTopics_when_validCategoryAndAccessGrandet(self):
        category = td.fake_category()
        current_user = td.fake_user()
        paginated_topics = fake_topicsResponseModel()
        expected_response = (category, paginated_topics)
        
        with patch('services.categories_service.get_by_name') as mock_get_by_name:
            mock_get_by_name.return_value = category

            with patch('services.categories_service.validate_category_access') as mock_validation:
                mock_validation.return_value = True
    
                with patch('services.categories_service.get_topics_in_category') as mock_topics:
                    mock_topics.return_value = expected_response

                    result = categories.get_category(name=category.name, current_user=current_user)

        result_category, result_paginated_topics = result

        self.assertEqual(len(result_paginated_topics), len(paginated_topics.topics))
        self.assertEqual(result_category.name, category.name)
        self.assertEqual(result_category.is_private, category.is_private)
        self.assertEqual(result_category.is_locked, category.is_locked)

        for actual_topic, expected_topic in zip(result_paginated_topics[1].topics, paginated_topics.topics):
            self.assertEqual(actual_topic.id, expected_topic.id)
            self.assertEqual(actual_topic.text, expected_topic.text)
            self.assertEqual(actual_topic.user_email, expected_topic.user_email)
            self.assertEqual(actual_topic.category_name, expected_topic.category_name)
            self.assertEqual(actual_topic.title, expected_topic.title)
            self.assertEqual(actual_topic.is_locked, expected_topic.is_locked)

        self.assertEqual(result_paginated_topics[1].pagination, paginated_topics.pagination)
        mock_get_by_name.assert_called_once()
        mock_validation.assert_called_once()
        mock_topics.assert_called_once()
   
    def test_postCategories_returnsConclict_when_categoryAlreadyExist(self):
        current_user = td.fake_user(role='admin')
        category = td.fake_category()
        expected_response = td.fake_response(exact_response=Conflict,status_code=409, content='')

        with patch('services.categories_service.exists') as mock_exists:
            mock_exists.return_value = True
            result = categories.post_categories(category=category,_=current_user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Conflict)
        mock_exists.assert_called_once()


    def test_postCategories_successfullyCreatesCategory_when_validParams(self):
        current_user = td.fake_user(role='admin')
        category = td.fake_category()
        expected_response = f"Category with name: {category.name}, was created as {category.is_private}"

        with patch('services.categories_service.exists') as mock_exists:
            mock_exists.return_value = False

            with patch('services.categories_service.create_category') as mock_create_category:
                mock_create_category.return_value = expected_response

                result = categories.post_categories(category=category,_=current_user)

        self.assertEqual(result, expected_response)
        mock_exists.assert_called_once()
        mock_create_category.assert_called_once()

    def test_updateCategoryPrivacy_successfullyCreatesCategory_when_validParams(self):
        current_user = td.fake_user(role='admin')
        category = td.fake_category()
        expected_response =f"Category '{category.name}' was updated as: {category.is_private}"

        with patch('services.categories_service.patch_category') as mock_patch_category:
            mock_patch_category.return_value = expected_response

            result = categories.update_category_privacy(category=category,_=current_user)

        self.assertEqual(result, expected_response)
        mock_patch_category.assert_called_once()


    def test_lockCategory_returnsNotFound_whenCategoryDoesNotExist(self):
        category_name = "nonexistent_category"
        expected_response = td.fake_response(exact_response=NotFound,status_code=404, content=f'Category with name {category_name} does not exist')

        with patch('services.categories_service.get_by_name') as mock_get_by_name:
            mock_get_by_name.return_value = None

            result = categories.lock_category(category_name=category_name, is_locked='locked')

        self.assertIsInstance(result, NotFound)
        self.assertEqual(result.status_code, expected_response.status_code)
        mock_get_by_name.assert_called_once()

    def test_lockCategory_returnsConflict_whenCategoryAlreadyLocked(self):
        category_name = "locked_category"
        category = td.fake_category(is_locked='locked')
        expected_response = td.fake_response(exact_response=Conflict,status_code=409, content='Category already locked!')

        with patch('services.categories_service.get_by_name') as mock_get_by_name:
            mock_get_by_name.return_value = category

            result = categories.lock_category(category_name=category_name, is_locked='locked')

        self.assertIsInstance(result, Conflict)
        self.assertEqual(result.status_code, expected_response.status_code)
        mock_get_by_name.assert_called_once()

    def test_lockCategory_returnsConflict_whenCategoryAlreadyUnlocked(self):
        category_name = "unlocked_category"
        category = td.fake_category(is_locked='unlocked')
        expected_response = td.fake_response(exact_response=Conflict,status_code=409, content='Category already locked!')

        with patch('services.categories_service.get_by_name') as mock_get_by_name:
            mock_get_by_name.return_value = category

            result = categories.lock_category(category_name=category_name, is_locked='unlocked')

        self.assertIsInstance(result, Conflict)
        self.assertEqual(result.status_code, expected_response.status_code)
        mock_get_by_name.assert_called_once()

    def test_lockCategory_successfullyLocksCategory_whenValidParams(self):
        category_name = "category_to_lock"
        category = td.fake_category(is_locked='unlocked')
        expected_response = f"Category '{category_name}' was locked"

        with patch('services.categories_service.get_by_name') as mock_get_by_name:
            mock_get_by_name.return_value = category

            with patch('services.categories_service.lock') as mock_lock:
                mock_lock.return_value = expected_response

                result = categories.lock_category(category_name=category_name, is_locked='locked')

         
        self.assertEqual(result, expected_response)
        mock_get_by_name.assert_called_once()
        mock_lock.assert_called_once()