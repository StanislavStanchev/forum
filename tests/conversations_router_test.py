import unittest
import test_database as td
from unittest.mock import patch, Mock
from data.models import MessageData, Message
from common.responses import Forbidden, NotFound, Conflict
from routers import conversations as conversations_router


class TopicRouter_Should(unittest.TestCase):


    def test_create_conversation_createsMessage_when_allChecksPass(self):
        data = td.fake_messageData()
        user = td.fake_user()
        receiver = td.fake_user(email='vankata@gmail.com')
        message = td.fake_message(text=data.text, sender=user.email, receiver=data.receiver_email)
        

        with patch('services.users_service.get_by_email', return_value=receiver) as mock_get_by_email, \
            patch('services.conversations_service.create', return_value=message) as mock_create:

            result = conversations_router.create_conversation(data, user)

        self.assertEqual(result, message)
        mock_get_by_email.assert_called_once_with(data.receiver_email)
        mock_create.assert_called_once_with(user.email, data.receiver_email, data.text)
    
    def test_create_conversation_returnsNotFound_when_MessageDoesNotExists(self):
        data = td.fake_messageData()
        user = td.fake_user()
        
        with patch('services.users_service.get_by_email', return_value=False) as mock_get_by_email:

            result = conversations_router.create_conversation(data, user)

        self.assertIsInstance(result, NotFound)
        mock_get_by_email.assert_called_once_with(data.receiver_email)
















