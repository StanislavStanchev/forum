import unittest
import test_database as td
from routers import replies
from unittest.mock import patch
from common.responses import Forbidden, NotFound, Conflict, NoContent


class RepliesRouter_Should(unittest.TestCase):

    def test_getReplies_returnsListOfReplies(self):
        current_user = td.fake_user()
        reply1 = td.fake_reply()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        expected_response =[reply1, reply2]
                
        with patch('services.replies_service.all') as mock_replies:
            mock_replies.return_value = expected_response

            result = replies.get_replies(current_user=current_user)

        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)
        
        mock_replies.assert_called_once()

    def test_getReplies_returnsListOfReplies_when_emailFiltering(self):
        user = td.fake_user()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        expected_response = [reply2] 

        with patch('services.replies_service.all') as mock_replies:
            mock_replies.return_value = expected_response

            result = replies.get_replies(current_user=user, email_for_filtering='user@abv.bg')

        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)
        mock_replies.assert_called_once()

    def test_getReplies_returnsReplyResponseModel_when_search(self):
        user = td.fake_user()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        expected_response = [reply2] 

        with patch('services.replies_service.all') as mock_replies:
            mock_replies.return_value = expected_response

            result = replies.get_replies(current_user=user, search='Two')

        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)
        mock_replies.assert_called_once()

    def test_getReplies_returnsReplyResponseModel_with_emailFiltering_and_search(self):
        user = td.fake_user()
        reply2 = td.fake_reply(id=2, text='Two', topic_id=2, user_email='user@abv.bg', best_reply=True, upvotes=3, downvotes=2)
        expected_response = [reply2] 

        with patch('services.replies_service.all') as mock_replies:
            mock_replies.return_value = expected_response

            result = replies.get_replies(current_user=user, search='Two', email_for_filtering='user@abv.bg')

        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.id, expected_response.id)
            self.assertEqual(result.text, expected_response.text)
            self.assertEqual(result.user_email, expected_response.user_email)
            self.assertEqual(result.best_reply, expected_response.best_reply)
        mock_replies.assert_called_once()

    def test_getReplyById_returnsNotFound_when_InvlaidID(self):
        user = td.fake_user()
        expected_response = td.fake_response(exact_response=NotFound,status_code=404, content='')

        with patch('services.replies_service.get_by_id') as mock_reply:
            mock_reply.return_value = None

            result = replies.get_reply_by_id(current_user=user, id=120)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, NotFound)
        mock_reply.assert_called_once()

    def test_getReplyById_returnsForbidden_when_notMemberOrRestricted(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = td.fake_response(exact_response=Forbidden,status_code=403,content=Forbidden(content=f''))

        with patch('services.replies_service.get_by_id') as mock_reply:
            mock_reply.return_value = reply

            with patch('services.categories_service.validate_category_access') as mock_access:
                mock_access.return_value = expected_response

                result = replies.get_reply_by_id(current_user=user, id=120)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_reply.assert_called_once()
        mock_access.assert_called_once()

    def test_getReplyById_returnsSuccessfully_when_replyExistsAndUserHasAccess(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = reply

        with patch('services.replies_service.get_by_id') as mock_reply:
            mock_reply.return_value = reply

            with patch('services.categories_service.validate_category_access') as mock_access:
                mock_access.return_value = True

                result = replies.get_reply_by_id(current_user=user, id=120)

        for actual_reply, expected in zip(result, expected_response):
            self.assertEqual(actual_reply.id, expected.id)
            self.assertEqual(actual_reply.is_text, expected.text)
            self.assertEqual(actual_reply.user_email, expected.user_email)
            self.assertEqual(actual_reply.best_reply, expected.best_reply)
            self.assertEqual(actual_reply.upvotes, expected.upvotes)
            self.assertEqual(actual_reply.downvotes, expected.downvotes)

        mock_reply.assert_called_once()
        mock_access.assert_called_once()

    def test_createReply_returnsConflict_when_topicIsLocked(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = td.fake_response(exact_response=Conflict,status_code=409, content='')
            
        with patch('services.topics_service.topic_is_locked') as mock_topic_is_locked:
            mock_topic_is_locked.return_value = True
            result = replies.create_reply(current_user=user, reply=reply)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Conflict)
        mock_topic_is_locked.assert_called_once()

    def test_createReply_returnsNotFound_when_topicDoesNotExist(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = td.fake_response(exact_response=NotFound,status_code=404, content='')
            
        with patch('services.topics_service.topic_is_locked') as mock_topic_is_locked:
            mock_topic_is_locked.return_value = False

            with patch('services.topics_service.topic_exists') as mock_topic_exists:
                mock_topic_exists.return_value = False
                result = replies.create_reply(current_user=user, reply=reply)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, NotFound)
        mock_topic_is_locked.assert_called_once()
        mock_topic_exists.assert_called_once()

    def test_createReply_returnsForbidden_when_notMemberOrRestricted(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = td.fake_response(exact_response=Forbidden,status_code=403,content=Forbidden(content=f''))
        
        with patch('services.topics_service.topic_is_locked') as mock_topic_is_locked:
            mock_topic_is_locked.return_value = False

            with patch('services.topics_service.topic_exists') as mock_topic_exists:
                mock_topic_exists.return_value = True

                with patch('services.categories_service.validate_category_access') as mock_access:
                    mock_access.return_value = expected_response
                    result = replies.create_reply(current_user=user, reply=reply)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_topic_is_locked.assert_called_once()
        mock_access.assert_called_once()
        mock_topic_exists.assert_called_once()
        
    def test_createReply_createsSuccessfully_when_validArgs(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = reply
        
        with patch('services.topics_service.topic_is_locked') as mock_topic_is_locked:
            mock_topic_is_locked.return_value = False
            with patch('services.topics_service.topic_exists') as mock_topic_exists:
                mock_topic_exists.return_value = True
                with patch('services.categories_service.validate_category_access') as mock_access:
                    mock_access.return_value = True       
                    with patch('services.replies_service.create') as mock_reply:
                        mock_reply.return_value = expected_response
                        result = replies.create_reply(current_user=user, reply=reply)
        
        for actual_reply, expected in zip(result, expected_response):
            self.assertEqual(actual_reply.id, expected.id)
            self.assertEqual(actual_reply.is_text, expected.text)
            self.assertEqual(actual_reply.user_email, expected.user_email)
            self.assertEqual(actual_reply.best_reply, expected.best_reply)
            self.assertEqual(actual_reply.upvotes, expected.upvotes)
            self.assertEqual(actual_reply.downvotes, expected.downvotes)

        mock_topic_is_locked.assert_called_once()
        mock_access.assert_called_once()
        mock_topic_exists.assert_called_once()
        mock_reply.assert_called_once()

    def test_delete_reply_deletesSuccessfully_when_author(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = td.fake_response(exact_response=NoContent, status_code=204, content='')

        with patch('services.replies_service.get_by_id') as mock_get_reply:
            mock_get_reply.return_value = reply
            with patch('services.categories_service.validate_category_access') as mock_validate_access:
                mock_validate_access.return_value = True
                with patch('services.replies_service.delete') as mock_delete:
                    mock_delete.return_value = None
                    result = replies.delete_reply(id=reply.id, current_user=user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, NoContent )
        mock_get_reply.assert_called_once_with(reply.id)
        mock_validate_access.assert_called_once()
        mock_delete.assert_called_once()


    def test_delete_reply_returnsNotFound_when_replyDoesNotExist(self):
        user = td.fake_user()
        reply = td.fake_reply()
        expected_response = td.fake_response(exact_response=NotFound,status_code=404, content='')

        with patch('services.replies_service.get_by_id') as mock_get_reply:
            mock_get_reply.return_value = False
            result = replies.delete_reply(id=reply.id, current_user=user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, NotFound)
        mock_get_reply.assert_called_once_with(reply.id)

    def test_delete_reply_returnsForbidden_when_notAuthor(self):
        user = td.fake_user(email='gosho@abv.bg')
        reply = td.fake_reply(user_email='pesho@abv.bg')
        expected_response = td.fake_response(exact_response=Forbidden,status_code=403, content='')

        with patch('services.replies_service.get_by_id') as mock_get_reply:
            mock_get_reply.return_value = reply
            result = replies.delete_reply(id=reply.id, current_user=user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_get_reply.assert_called_once_with(reply.id)

    def test_delete_reply_returnsForbidden_when_categoryAccessForbidden(self):
        user = td.fake_user(email='gosho@abv.bg')
        reply = td.fake_reply(user_email='gosho@abv.bg')
        expected_response = td.fake_response(exact_response=Forbidden,status_code=403, content='')

        with patch('services.replies_service.get_by_id') as mock_get_reply:
            mock_get_reply.return_value = reply
            with patch('services.categories_service.validate_category_access') as mock_validate_access:
                mock_validate_access.return_value = expected_response
                result = replies.delete_reply(id=reply.id, current_user=user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_get_reply.assert_called_once_with(reply.id)
        mock_validate_access.assert_called_once_with(user, topic_id=reply.topic_id, required_access='full')