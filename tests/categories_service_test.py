import unittest
import test_database as td
from services import categories_service
from unittest.mock import Mock, patch
from data.models import Category, UserAccessForCategory
from common.responses import Forbidden, NotFound, Conflict

class CategoriesService_Should(unittest.TestCase):

    def test_isPrivate_returnsTrue_when_categoryIsPrivate(self):
        get_data_func = lambda q, params: [(1)]
        result = categories_service.is_private('Example category', get_data_func)
        expected = True
        self.assertEqual(expected, result)

    def test_isPrivate_returnsFalse_when_categoryIsNotPrivate(self):
        get_data_func = lambda q, params: []
        result = categories_service.is_private('Example category', get_data_func)
        expected = False
        self.assertEqual(expected, result)

    def test_isCategoryMember_returnsTrue_when_userIsMember(self):
        user = td.fake_user()
        category = td.fake_category()
        get_data_func = lambda q, params: [(user.username, category.name, 1)]
        result = categories_service.is_category_member(user=user, category=category, get_data_func=get_data_func)
        self.assertTrue(result)

    def test_isCategoryMember_returnsTrue_when_userEmailIsMember(self):
        user = td.fake_user()
        category = td.fake_category()
        get_data_func = lambda q, params: [(user.email, category.name, 1)]
        result = categories_service.is_category_member(user_email=user.email, category_name=category.name, get_data_func=get_data_func)
        self.assertTrue(result)

    def test_isCategoryMember_returnsFalse_when_userIsNotMember(self):
        user = td.fake_user()
        category = td.fake_category()
        get_data_func = lambda q, params: []
        result = categories_service.is_category_member(user=user, category=category, get_data_func=get_data_func)
        self.assertFalse(result)

    def test_isCategoryMember_returnsFalse_when_userEmailIsNotMember(self):
        user = td.fake_user()
        category = td.fake_category()
        get_data_func = lambda q, params: []
        result = categories_service.is_category_member(user_email=user.email, category_name=category.name, get_data_func=get_data_func)
        self.assertFalse(result)

    def test_exists_returnsTrue_when_categoryExists(self):
        category = td.fake_category()
        get_data_func = lambda query, params: [(category.name,)]
        result = categories_service.exists(category.name, get_data_func)
        self.assertTrue(result)

    def test_exists_returnsFalse_when_categoryDoesNotExist(self):
        category = td.fake_category()
        get_data_func = lambda query, params: []
        result = categories_service.exists(category.name, get_data_func)
        self.assertFalse(result)

    def test_sort_categories_sortsListByName(self):
        categories = [
            td.fake_category(name='cab'),
            td.fake_category(name='abc'),
            td.fake_category(name='bca')]
        expected_result = [
            td.fake_category(name='abc'),
            td.fake_category(name='bca'),
            td.fake_category(name='cab')]
        result = categories_service.sort_categories(categories)
        result_names = [cat.name for cat in result]
        expected_names = [cat.name for cat in expected_result]
        self.assertEqual(result_names, expected_names)

    def test_sort_categories_sortsListByNameInReverse(self):
        categories = [
            td.fake_category(name='cab'),
            td.fake_category(name='abc'),
            td.fake_category(name='bca')]
        expected_result = [
            td.fake_category(name='cab'),
            td.fake_category(name='bca'),
            td.fake_category(name='abc')]
        result = categories_service.sort_categories(categories, reverse=True)
        result_names = [cat.name for cat in result]
        expected_names = [cat.name for cat in expected_result]
        self.assertEqual(result_names, expected_names)

    def test_all_returnsCategoryResponseModel(self):
        user = td.fake_user()
        category1 = td.fake_category(name='cat 1')
        category2 = td.fake_category(name='cat 2')
        pagination = td.fake_pagination()
        get_data_func = lambda query, params: [('cat 1', 0, 0), ('cat 2', 0, 0)]
        expected_response = td.fake_categoryResponseModel(categories=[category1, category2], pagination=pagination)

        with patch('services.categories_service.Category.from_query_result') as mock_from_query_result:
                mock_from_query_result.return_value = [category1, category2]

                with patch('services.categories_service.common.utils.paginate') as mock_paginate:
                    mock_paginate.return_value = expected_response

                    result = categories_service.all(current_user=user, get_data_func=get_data_func)

        self.assertEqual(len(result.categories), len(expected_response.categories))
        for actual_category, expected_category in zip(result.categories, expected_response.categories):
            self.assertEqual(actual_category.name, expected_category.name)
            self.assertEqual(actual_category.is_locked, expected_category.is_locked)
            self.assertEqual(actual_category.is_private, expected_category.is_private)

        self.assertEqual(result.pagination, expected_response.pagination)
        self.assertEqual(mock_from_query_result.call_count, 2)
        self.assertEqual(mock_paginate.call_count, 1)

    def test_all_appliesSearchFilter(self):
  
        user = td.fake_user()
        category = td.fake_category(name='cat 2')
        pagination = td.fake_pagination()
        get_data_func = lambda query, params: [('cat 1', 0, 0), ('cat 2', 0, 0)]
        expected_response = td.fake_categoryResponseModel(categories=[category], pagination=pagination)

        with patch('services.categories_service.Category.from_query_result') as mock_from_query_result:
                mock_from_query_result.return_value = [category]

                with patch('services.categories_service.common.utils.paginate') as mock_paginate:
                    mock_paginate.return_value = expected_response

                    result = categories_service.all(current_user=user, get_data_func=get_data_func, search='2')

        self.assertEqual(len(result.categories), len(expected_response.categories))
        for actual_category, expected_category in zip(result.categories, expected_response.categories):
            self.assertEqual(actual_category.name, expected_category.name)
            self.assertEqual(actual_category.is_locked, expected_category.is_locked)
            self.assertEqual(actual_category.is_private, expected_category.is_private)

        self.assertEqual(result.pagination, expected_response.pagination)
        self.assertEqual(mock_from_query_result.call_count, 2)
        self.assertEqual(mock_paginate.call_count, 1)

    def test_all_appliesSorting(self):

        user = td.fake_user()
        category1 = td.fake_category(name='cat 1')
        category2 = td.fake_category(name='cat 2')
        pagination = td.fake_pagination()
        get_data_func = lambda query, params: [('cat 2', 0, 0), ('cat 1', 0, 0)]
        expected_response = td.fake_categoryResponseModel(categories=[category1, category2], pagination=pagination)

        with patch('services.categories_service.Category.from_query_result') as mock_from_query_result:
                mock_from_query_result.return_value = [category2, category1]

                with patch('services.categories_service.common.utils.paginate') as mock_paginate:
                    mock_paginate.return_value = expected_response

                    result = categories_service.all(current_user=user, get_data_func=get_data_func, search='2')

        self.assertEqual(len(result.categories), len(expected_response.categories))
        for actual_category, expected_category in zip(result.categories, expected_response.categories):
            self.assertEqual(actual_category.name, expected_category.name)
            self.assertEqual(actual_category.is_locked, expected_category.is_locked)
            self.assertEqual(actual_category.is_private, expected_category.is_private)

        self.assertEqual(result.pagination, expected_response.pagination)
        self.assertEqual(mock_from_query_result.call_count, 2)
        self.assertEqual(mock_paginate.call_count, 1)


    def test_updateUserAccess_returnsUserAccessForCategory_when_isNotMember(self):
        user = td.fake_user() 
        access = 'full'
        category = td.fake_category()
        insert_data_func = lambda query, params: (1, user.email,category.name)
        expected_response = td.fake_userAccessForCategory()

        with patch('services.categories_service.is_category_member') as mock_is_category_member:
                mock_is_category_member.return_value = False

                result = categories_service.update_user_access(user=user,access_type=access,category=category, insert_data_func=insert_data_func)
        
        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.email, expected_response.email)
            self.assertEqual(result.category, expected_response.category)
            self.assertEqual(result.access, expected_response.access)
        
        self.assertEqual(mock_is_category_member.call_count, 1)
        self.assertIsInstance(result, UserAccessForCategory)

    def test_updateUserAccess_returnsUserAccessForCategory_when_isNotMember(self):
        user = td.fake_user() 
        access = 'full'
        category = td.fake_category()
        insert_data_func = lambda query, params: (1, user.email,category.name)
        expected_response = td.fake_userAccessForCategory()

        with patch('services.categories_service.is_category_member') as mock_is_category_member:
                mock_is_category_member.return_value = False

                result = categories_service.update_user_access(user=user,access_type=access,category=category, insert_data_func=insert_data_func)
        
        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.email, expected_response.email)
            self.assertEqual(result.category, expected_response.category)
            self.assertEqual(result.access, expected_response.access)
        
        self.assertEqual(mock_is_category_member.call_count, 1)
        self.assertIsInstance(result, UserAccessForCategory)

    def test_updateUserAccess_returnsUserAccessForCategory_when_isMember(self):
        user = td.fake_user() 
        access = 'read_only'
        category = td.fake_category()
        update_data_func = lambda query, params: (2, user.email, category.name)
        expected_response = td.fake_userAccessForCategory()

        with patch('services.categories_service.is_category_member') as mock_is_category_member:
                mock_is_category_member.return_value = True

                result = categories_service.update_user_access(user=user,access_type=access,category=category, update_data_func=update_data_func)
        
        for result, expected_response in zip(result, expected_response):
            self.assertEqual(result.email, expected_response.email)
            self.assertEqual(result.category, expected_response.category)
            self.assertEqual(result.access, expected_response.access)
        
        self.assertEqual(mock_is_category_member.call_count, 1)
        self.assertIsInstance(result, UserAccessForCategory)

    def test_getUserAccessForCategory_returnFullsUserAccessForCategory_when_isMember(self):
        user = td.fake_user()
        category =  td.fake_category()
        get_data_func = lambda query, params: [(3,)]
        expected_response = 'full'
        result = categories_service.get_user_access_for_category(user.email, category=category, get_data_func=get_data_func)
        self.assertEqual(result, expected_response)
        self.assertIsInstance(result, str)

    def test_getUserAccessForCategory_returnReadOnlyUserAccessForCategory_when_isMember(self):
        user = td.fake_user()
        category =  td.fake_category()
        get_data_func = lambda query, params: [(2,)]
        expected_response = 'read_only'
        result = categories_service.get_user_access_for_category(user.email, category=category, get_data_func=get_data_func)
        self.assertEqual(result, expected_response)
        self.assertIsInstance(result, str)

    def test_getUserAccessForCategory_returnRestrictedUserAccessForCategory_when_isMember(self):
        user = td.fake_user()
        category =  td.fake_category()
        get_data_func = lambda query, params: [(1,)]
        expected_response = 'restricted'
        result = categories_service.get_user_access_for_category(user.email, category=category, get_data_func=get_data_func)
        self.assertEqual(result, expected_response)
        self.assertIsInstance(result, str)

    def test_getUserAccessForCategory_returnForbidden_when_noAccess(self):
        user = td.fake_user()
        category = td.fake_category()
        get_data_func = lambda query, params: []
        mock_forbidden = td.fake_response(status_code=403, exact_response=Forbidden,
                                          content=f'You are not a category member of "{category.name}".')
        expected_response = mock_forbidden
        result = categories_service.get_user_access_for_category(user.email, category=category, get_data_func=get_data_func)
        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Forbidden)

    def test_validateCategoryAccess_hasAccess_when_publicCategory(self):
        user = td.fake_user()
        category_name = 'public_category'
        expected_result = (True, category_name)
        
        with patch('services.categories_service.is_private') as mock_is_private:
                mock_is_private.return_value = False
        
        result = categories_service.validate_category_access(user, category_name)

        self.assertEqual(result, expected_result)

    def test_validateCategoryAccess_noAccess_when_privateAndNotMember(self):
        user = td.fake_user()
        category_name = 'private_category'
        expected_mock_forbidden = td.fake_response(status_code=403, exact_response=Forbidden,
                                          content=f'You are not a category member of "{category_name}".')

        with patch('services.categories_service.is_private') as mock_is_private:
                mock_is_private.return_value = True
        
                with patch('services.categories_service.get_user_access_for_category') as mock_user_access:
                    mock_user_access.return_value = expected_mock_forbidden

                    result = categories_service.validate_category_access(user, category_name)

        self.assertEqual(result.status_code, expected_mock_forbidden.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_user_access.assert_called_once()
        mock_is_private.assert_called_once()

    def test_validateCategoryAccess_noAccess_when_privateAndRestrictedMember(self):
        user = td.fake_user()
        category_name = 'private_category'
        expected_mock_forbidden = td.fake_response(status_code=403, exact_response=Forbidden,
                                          content=f'You are not a category member of "{category_name}".')

        with patch('services.categories_service.is_private') as mock_is_private:
                mock_is_private.return_value = True
        
                with patch('services.categories_service.get_user_access_for_category') as mock_user_access:
                    mock_user_access.return_value = expected_mock_forbidden

                    result = categories_service.validate_category_access(user, category_name)
        
        self.assertEqual(result.status_code, expected_mock_forbidden.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_user_access.assert_called_once()
        mock_is_private.assert_called_once()

    def test_validateCategoryAccess_confirmsAccess_when_privateAndReadOnlyMember(self):
        user = td.fake_user()
        category_name = 'private_category'
        expected_response = (True, category_name)

        with patch('services.categories_service.is_private') as mock_is_private:
                mock_is_private.return_value = True
        
                with patch('services.categories_service.get_user_access_for_category') as mock_user_access:
                    mock_user_access.return_value = 'read_only'

                    result = categories_service.validate_category_access(user, category_name)

        self.assertEqual(result, expected_response)
        mock_user_access.assert_called_once()
        mock_is_private.assert_called_once()

    def test_validateCategoryAccess_confirmsAccess_when_privateAndMemberWithFullAccess(self):
        user = td.fake_user()
        category_name = 'private_category'
        expected_response = (True, category_name)

        with patch('services.categories_service.is_private') as mock_is_private:
                mock_is_private.return_value = True
        
                with patch('services.categories_service.get_user_access_for_category') as mock_user_access:
                    mock_user_access.return_value = 'full'

                    result = categories_service.validate_category_access(user, category_name)

        self.assertEqual(result, expected_response)
        mock_is_private.assert_called_once()

    def test_validateCategoryAccess_confirmsTopicAccess_when_privatendMemberWithFullAccessA(self):
        user = td.fake_user()
        get_data_func = lambda query, params: [('private_category',)]
        expected_response = (True, 'private_category')

        with patch('services.categories_service.is_private') as mock_is_private:
                mock_is_private.return_value = True
        
                with patch('services.categories_service.get_user_access_for_category') as mock_user_access:
                    mock_user_access.return_value = 'full'

                    result = categories_service.validate_category_access(user, topic_id=1, get_data_func=get_data_func)

        self.assertEqual(result, expected_response)
        mock_user_access.assert_called_once()

    def test_validateCategoryAccess_confirmsReplyAccess_when_privatendMemberWithFullAccessA(self):
        user = td.fake_user()
        get_data_func = lambda query, params: [('private_category',)]
        expected_response = (True, 'private_category')

        with patch('services.categories_service.is_private') as mock_is_private:
                mock_is_private.return_value = True
        
                with patch('services.categories_service.get_user_access_for_category') as mock_user_access:
                    mock_user_access.return_value = 'full'

                    result = categories_service.validate_category_access(user, reply_id=1, get_data_func=get_data_func)

        self.assertEqual(result, expected_response)
        mock_user_access.assert_called_once()

    def test_createCategory_successfullyCreatesPrivateCategory_when_validArgs(self):
        category = td.fake_category(is_private=1)
        expected_response = f"Category with name: {category.name}, was created as {category.is_private}"
        insert_data_func_mock = Mock()
        
        with patch('services.categories_service.insert_query', insert_data_func_mock):
            result = categories_service.create_category(category)

        self.assertEqual(result, expected_response)
        insert_data_func_mock.assert_called_once()
        insert_data_func_mock.reset_mock()

    def test_createCategory_successfullyCreatesPublicCategory_when_validArgs(self):
        category = td.fake_category()
        expected_response = f"Category with name: {category.name}, was created as {category.is_private}"
        insert_data_func_mock = Mock()
        
        with patch('services.categories_service.insert_query', insert_data_func_mock):
            result = categories_service.create_category(category)

        self.assertEqual(result, expected_response)
        insert_data_func_mock.assert_called_once()
        insert_data_func_mock.reset_mock()

    def test_patchCategory_raisesConflict_when_categoryDoesNotExist(self):
        category = td.fake_category()
        expected_mock_not_found = td.fake_response(status_code=404, exact_response=NotFound,content="Category does not exist!")

        with patch('services.categories_service.read_query') as mock_read_query:
                mock_read_query.return_value = []
        
                result = categories_service.patch_category(category)
        
        self.assertEqual(result.status_code, expected_mock_not_found.status_code)
        self.assertIsInstance(result, NotFound)
        mock_read_query.assert_called_once()

    def test_patchCategory_raisesConflict_when_categoryAlreadyPublic(self):
        category = td.fake_category(is_private='public')
        expected_mock_not_found = td.fake_response(status_code=409, exact_response=Conflict,content="You can not with same private level!")

        with patch('services.categories_service.read_query') as mock_read_query:
                mock_read_query.return_value = [('category', 0)]
                result = categories_service.patch_category(category)
        
        self.assertEqual(result.status_code, expected_mock_not_found.status_code)
        self.assertIsInstance(result, Conflict)
        mock_read_query.assert_called_once()

    def test_patchCategory_raisesConflict_when_categoryAlreadyPrivate(self):
        category = td.fake_category(is_private='private')
        expected_mock_not_found = td.fake_response(status_code=409, exact_response=Conflict,content="You can not with same private level!")

        with patch('services.categories_service.read_query') as mock_read_query:
                mock_read_query.return_value = [('category', 1)]
                result = categories_service.patch_category(category)
        
        self.assertEqual(result.status_code, expected_mock_not_found.status_code)
        self.assertIsInstance(result, Conflict)
        mock_read_query.assert_called_once()

    def test_patchCategory_successfullyUpdatesCategory_when_categoryExist(self):
        category = td.fake_category(is_private='private')
        expected_response = f"Category '{category.name}' was updated as: private"

        with patch('services.categories_service.read_query') as mock_read_query:
                mock_read_query.return_value = [('category', 0, 0)]
                
                with patch('services.categories_service.update_query') as mock_update_query:
                        mock_update_query.return_value = 1 
            
                        result = categories_service.patch_category(category)
        
        self.assertEqual(result, expected_response)
        mock_read_query.assert_called_once()
        mock_update_query.assert_called_once()

    def test_lock_successfullyLocksCategory(self):
        category = td.fake_category(is_locked='unlocked')
        expected_response = 'locked'
                
        with patch('services.categories_service.update_query') as mock_update_query:
                mock_update_query.return_value = 1 
    
                result = categories_service.lock(category, is_locked='locked')

        self.assertEqual(result.is_locked, expected_response)
        self.assertIsInstance(result, Category)
        mock_update_query.assert_called_once()
        
    def test_lock_successfullyUnlocksCategory(self):
        category = td.fake_category()
        expected_response = 'unlocked'
                
        with patch('services.categories_service.update_query') as mock_update_query:
                mock_update_query.return_value = 1 
    
                result = categories_service.lock(category, is_locked='unlocked')

        self.assertEqual(result.is_locked, expected_response)
        self.assertIsInstance(result, Category)
        mock_update_query.assert_called_once()

    def test_categoryIsLocked_returnsTrue_when_Locked(self):
 
        with patch('services.categories_service.read_query') as mock_read_query:
                mock_read_query.return_value = [(1,)]

                result = categories_service.category_is_locked('example category') 
        
        self.assertTrue(result)
        mock_read_query.assert_called_once()
   
    def test_categoryIsLocked_returnsFalse_when_Unlocked(self):
 
        with patch('services.categories_service.read_query') as mock_read_query:
                mock_read_query.return_value = []

                result = categories_service.category_is_locked('example category') 
        
        self.assertFalse(result)
        mock_read_query.assert_called_once()