import unittest
import test_database as td
from routers import users
from unittest.mock import patch
from data.models import UserAccessForCategory
from common.responses import NotFound, Conflict


class CategoriesRouter_Should(unittest.TestCase):

    def test_updateCategoryMemberAccess_returnsNotFound_when_userDoesNotExist(self):
        current_user = td.fake_user()
        email = "non-existing@abv.bg"
        expected_response = td.fake_response(exact_response=NotFound,status_code=404, 
                                             content=f"User with email:{email} does not exist.")

        with patch('services.users_service.get_by_email') as mock_get_by_name:
            mock_get_by_name.return_value = None
    
            result = users.update_category_member_access(user_email=email, access_type='full', _=current_user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, NotFound)
        mock_get_by_name.assert_called_once()


    def test_updateCategoryMemberAccess_returnsNotFound_when_categoryDoesNotExist(self):
        current_user = td.fake_user(role='admin')
        email = "admin@abv.bg"
        name = "non-existing-category"
        expected_response = td.fake_response(exact_response=NotFound,status_code=404, 
                                             content=f"Category with name: {name} does not exist.")

        with patch('services.users_service.get_by_email') as mock_get_by_email:
            mock_get_by_email.return_value = current_user
            with patch('services.categories_service.get_by_name') as mock_get_by_name:
                mock_get_by_name.return_value = None
                result = users.update_category_member_access(user_email=email, category_name=name,access_type='full', _=current_user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, NotFound)
        mock_get_by_name.assert_called_once()
        mock_get_by_email.assert_called_once()

    def test_updateCategoryMemberAccess_returnConflict_when_adminAndAccessIsNotFull(self):
        current_user = td.fake_user(role='admin')
        email = "admin@abv.bg"
        name = "category"
        expected_response = td.fake_response(exact_response=Conflict,status_code=409, content='')

        with patch('services.users_service.get_by_email') as mock_get_by_email:
            mock_get_by_email.return_value = current_user
            with patch('services.categories_service.get_by_name') as mock_get_by_name:
                mock_get_by_name.return_value = True
                result = users.update_category_member_access(user_email=email, category_name=name,access_type='read_only', _=current_user)

        self.assertEqual(result.status_code, expected_response.status_code)
        self.assertIsInstance(result, Conflict)
        mock_get_by_name.assert_called_once()
        mock_get_by_email.assert_called_once()

    def test_updateCategoryMemberAccess_callsUpdateUserAccess_when_validParams(self):
        current_user = td.fake_user(role='admin')
        email = "admin@abv.bg"
        name = "category"
        expected_response = td.fake_user_access_for_category()

        with patch('services.users_service.get_by_email') as mock_get_by_email:
            mock_get_by_email.return_value = current_user
            with patch('services.categories_service.get_by_name') as mock_get_by_name:
                mock_get_by_name.return_value = True
                with patch('services.categories_service.update_user_access') as mock_update_user_access:
                    mock_update_user_access.return_value = expected_response
            
                    result = users.update_category_member_access(user_email=email, category_name=name,access_type='full', _=current_user)

        self.assertEqual(result, expected_response)
        self.assertIsInstance(result,UserAccessForCategory )
        mock_get_by_name.assert_called_once()
        mock_get_by_email.assert_called_once()
        mock_update_user_access.assert_called_once()