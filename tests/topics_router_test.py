import unittest
import test_database as td
from unittest.mock import patch, Mock, DEFAULT
from data.models import Topic
from common.responses import Forbidden, NotFound, Conflict
from routers import topics as topics_router


class TopicRouter_Should(unittest.TestCase):

    def test_create_topic_returnsConflict_when_categoryIsLocked(self):
        current_user = td.fake_user()
        topic = td.fake_topic()

        with patch('services.categories_service.category_is_locked') as mock_category_is_locked:
            mock_category_is_locked.return_value = True

            result = topics_router.create_topic(topic, current_user)

        self.assertIsInstance(result, Conflict)
        mock_category_is_locked.assert_called_once_with(topic.category_name)

    def test_create_topic_returnsNotFound_when_categoryDoesNotExist(self):
        current_user = td.fake_user()
        topic = td.fake_topic()

        with patch('services.categories_service.category_is_locked') as mock_category_is_locked:
            mock_category_is_locked.return_value = False

            with patch('services.categories_service.exists') as mock_exists:
                mock_exists.return_value = False

                result = topics_router.create_topic(topic, current_user)

        self.assertIsInstance(result, NotFound)
        mock_category_is_locked.assert_called_once_with(topic.category_name)
        mock_exists.assert_called_once_with(topic.category_name)

    def test_create_topic_returnsForbidden_when_userDoesNotHaveCategoryAccess(self):
        current_user = td.fake_user()
        topic = td.fake_topic()

        with patch.multiple('services.categories_service',
                            category_is_locked=DEFAULT,
                            exists=DEFAULT,
                            validate_category_access=DEFAULT) as mock_services:

            mock_services['category_is_locked'].return_value = False
            mock_services['exists'].return_value = True
            mock_services['validate_category_access'].return_value = Forbidden()

            result = topics_router.create_topic(topic, current_user)

            self.assertIsInstance(result, Forbidden)
            mock_services['category_is_locked'].assert_called_once_with(topic.category_name)
            mock_services['exists'].assert_called_once_with(topic.category_name)
            mock_services['validate_category_access'].assert_called_once_with(current_user, category_name=topic.category_name)


    def test_create_topic_createsTopic_when_allChecksPass(self):
        current_user = td.fake_user()
        topic = td.fake_topic()
        created_topic = td.fake_topic()

        with patch('services.categories_service.category_is_locked', return_value=False) as mock_category_is_locked, \
            patch('services.categories_service.exists', return_value=True) as mock_exists, \
            patch('services.categories_service.validate_category_access', return_value=True) as mock_validate_category_access, \
            patch('services.topics_service.create', return_value=created_topic) as mock_create:

            result = topics_router.create_topic(topic, current_user)

        self.assertEqual(result, created_topic)
        mock_category_is_locked.assert_called_once_with(topic.category_name)
        mock_exists.assert_called_once_with(topic.category_name)
        mock_validate_category_access.assert_called_once_with(current_user, category_name=topic.category_name)
        mock_create.assert_called_once_with(topic, current_user)

    def test_get_topics_returnsPaginatedTopics(self):

        current_user = td.fake_user()
        all_topics = [td.fake_topic() for _ in range(10)]
        paginated_topics = all_topics[:5]

        with patch('services.topics_service.wanted_topics') as mock_wanted_topics, \
            patch('common.utils.paginate') as mock_paginate:
            
            mock_wanted_topics.return_value = all_topics
            mock_paginate.return_value = paginated_topics

            result = topics_router.get_topics(sort=None, search=None, sort_by=None, page=1, items_per_page=5, current_user=current_user)

        self.assertEqual(result, paginated_topics)
        mock_wanted_topics.assert_called_once_with(sort=None, search=None, sort_by=None, current_user=current_user)
        mock_paginate.assert_called_once_with(all_topics, 1, 5)
    

    def test_get_by_id_returnsTopicAndReplies_when_userHasCategoryAccess_and_topicAndRepliesExist(self):

        current_user = td.fake_user()
        topic_id = 1
        topic = td.fake_topic()
        replies = [td.fake_reply() for _ in range(3)]

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.replies_service.get_by_topic_id', return_value=replies) as mock_get_replies, \
            patch('services.categories_service.validate_category_access', return_value=True) as mock_validate_category_access:

            result = topics_router.get_by_id(id=topic_id, current_user=current_user)

        self.assertEqual(result, (topic, replies))
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_get_replies.assert_called_once_with(topic_id)
        mock_validate_category_access.assert_called_once_with(current_user, topic_id=topic_id)


    def test_get_by_id_returnsForbidden_when_userDoesNotHaveCategoryAccess(self):
        current_user = td.fake_user()
        topic_id = 1
        topic = td.fake_topic()
        forbidden_response = Forbidden()

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.categories_service.validate_category_access', return_value=forbidden_response) as mock_validate_category_access:

            result = topics_router.get_by_id(topic_id, current_user)

        self.assertIsInstance(result, Forbidden)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_validate_category_access.assert_called_once_with(current_user, topic_id=topic.id)
        

    def test_get_by_id_returnsNotFound_when_topicDoesNotExist(self):
        current_user = td.fake_user()
        topic_id = 1

        with patch('services.topics_service.get_topic_by_id') as mock_get_topic_by_id:
            mock_get_topic_by_id.return_value = None

            result = topics_router.get_by_id(topic_id, current_user=current_user)

        self.assertIsInstance(result, NotFound)
        mock_get_topic_by_id.assert_called_once_with(topic_id)

    def test_choose_best_reply_returnsNotFound_when_topicDoesNotExist(self):
        current_user = td.fake_user()
        topic_id = 1
        reply_id = 1
        best_reply_flag = True  

        with patch('services.topics_service.get_topic_by_id', return_value=None) as mock_get_topic_by_id:

            result = topics_router.choose_best_reply(topic_id, reply_id, best_reply_flag, current_user)

        self.assertIsInstance(result, NotFound)
        mock_get_topic_by_id.assert_called_once_with(topic_id)


    def test_choose_best_reply_returnsNotFound_when_replyDoesNotExist(self):
        current_user = td.fake_user()
        topic_id = 1
        topic = td.fake_topic()
        reply_id = 1
        best_reply_flag = True

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.replies_service.get_by_id', return_value=None) as mock_get_reply:

            result = topics_router.choose_best_reply(topic_id, reply_id, best_reply_flag, current_user)

        self.assertIsInstance(result, NotFound)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_get_reply.assert_called_once_with(reply_id)


    def test_choose_best_reply_returnsForbidden_when_userDoesNotHaveCategoryAccess(self):
        current_user = td.fake_user()
        topic_id = 1
        topic = td.fake_topic()
        reply_id = 1
        reply = td.fake_reply()
        best_reply_flag = True

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.replies_service.get_by_id', return_value=reply) as mock_get_reply, \
            patch('services.categories_service.validate_category_access', return_value=Forbidden()) as mock_validate_category_access:

            result = topics_router.choose_best_reply(topic_id, reply_id, best_reply_flag, current_user)

        self.assertIsInstance(result, Forbidden)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_get_reply.assert_called_once_with(reply_id)
        mock_validate_category_access.assert_called_once_with(current_user, topic_id=topic_id, required_access='full')


    def test_choose_best_reply_returnsForbidden_when_userIsNotTopicAuthor(self):
        current_user = td.fake_user()
        topic_id = 1
        topic = td.fake_topic(user_email='another_user@email.com')
        reply_id = 1
        reply = td.fake_reply()
        best_reply_flag = True 

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.replies_service.get_by_id', return_value=reply) as mock_get_reply, \
            patch('services.categories_service.validate_category_access', return_value=True) as mock_validate_category_access:

            result = topics_router.choose_best_reply(topic_id, reply_id, best_reply_flag, current_user)

        self.assertIsInstance(result, Forbidden)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_get_reply.assert_called_once_with(reply_id)
        mock_validate_category_access.assert_called

    def test_choose_best_reply_marksReplyAsBest_when_allChecksPass(self):
        current_user = td.fake_user()
        topic_id = 1
        topic = td.fake_topic(user_email=current_user.email)
        reply_id = 1
        reply = td.fake_reply()
        best_reply_flag = True
        marked_reply = td.fake_reply(best_reply=best_reply_flag)

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.replies_service.get_by_id', return_value=reply) as mock_get_reply, \
            patch('services.categories_service.validate_category_access', return_value=True) as mock_validate_category_access, \
            patch('services.replies_service.choose_best_reply', return_value=marked_reply) as mock_choose_best_reply:

            result = topics_router.choose_best_reply(topic_id, reply_id, best_reply_flag, current_user)

        self.assertEqual(result, marked_reply)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_get_reply.assert_called_once_with(reply_id)
        mock_validate_category_access.assert_called_once_with(current_user, topic_id=topic_id, required_access='full')
        mock_choose_best_reply.assert_called_once_with(reply, best_reply_flag)
    
    def test_lock_topic_returnsNotFound_when_topicDoesNotExist(self):
        topic_id = 1
        is_locked = 'locked'

        with patch('services.topics_service.get_topic_by_id', return_value=None) as mock_get_topic_by_id:
            result = topics_router.lock_topic(topic_id, is_locked)

        self.assertIsInstance(result, NotFound)
        mock_get_topic_by_id.assert_called_once_with(topic_id)

    def test_lock_topic_returnsConflict_when_topicIsAlreadyLocked(self):
        topic_id = 1
        is_locked = 'locked'
        topic = td.fake_topic(is_locked='locked')

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id:
            result = topics_router.lock_topic(topic_id, is_locked)

        self.assertIsInstance(result, Conflict)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
    
    def test_lock_topic_returnsConflict_when_topicIsAlreadyUnlocked(self):
        topic_id = 1
        is_locked = 'unlocked'
        topic = td.fake_topic(is_locked='unlocked')

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id:
            result = topics_router.lock_topic(topic_id, is_locked)

        self.assertIsInstance(result, Conflict)
        mock_get_topic_by_id.assert_called_once_with(topic_id)

    def test_lock_topic_locksTopic_when_allChecksPass(self):
        topic_id = 1
        is_locked = 'locked'
        topic = td.fake_topic()
        locked_topic = td.fake_topic(is_locked='locked')

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.topics_service.lock', return_value=locked_topic) as mock_lock:
            
            result = topics_router.lock_topic(topic_id, is_locked)

        self.assertEqual(result, locked_topic)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_lock.assert_called_once_with(topic, is_locked)

    def test_lock_topic_unlocksTopic_when_allChecksPass(self):
        topic_id = 1
        is_locked = 'unlocked'
        topic = td.fake_topic(is_locked='locked')
        unlocked_topic = td.fake_topic(is_locked='unlocked')

        with patch('services.topics_service.get_topic_by_id', return_value=topic) as mock_get_topic_by_id, \
            patch('services.topics_service.lock', return_value=unlocked_topic) as mock_lock:
            
            result = topics_router.lock_topic(topic_id, is_locked)

        self.assertEqual(result, unlocked_topic)
        mock_get_topic_by_id.assert_called_once_with(topic_id)
        mock_lock.assert_called_once_with(topic, is_locked)




        
    


