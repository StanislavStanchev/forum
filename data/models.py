from pydantic import BaseModel, EmailStr, conint, constr
from typing import Optional
from enum import Enum
import datetime


class LockedType(str, Enum):
    locked = 'locked'
    unlocked = 'unlocked'

class Topic(BaseModel):
    id: int | None
    text: str
    user_email: str | None
    category_name: str
    title: str
    is_locked: LockedType | None

    @classmethod
    def from_query_result(cls, id, text, user_email, title, category_name, is_locked):
        return cls(
            id=id,
            text=text,
            user_email=user_email,
            title=title,
            category_name=category_name,
            is_locked = 'locked' if is_locked == 1 else 'unlocked')
    
    def __eq__(self, other):
        if isinstance(other, Topic):
            return (self.id == other.id and
                    self.text == other.text and
                    self.user_email == other.user_email and
                    self.category_name == other.category_name and
                    self.title == other.title and
                    self.is_locked == other.is_locked)
        return False
    
class CreateReply(BaseModel):
    id: int | None
    text: constr(strip_whitespace=True, min_length=2, max_length=2000)
    topic_id: int
    user_email: EmailStr | None
    best_reply: bool = False

class ReplyResponseModel(CreateReply):
    id: int
    best_reply: bool
    upvotes: conint(ge=0)
    downvotes: conint(ge=0)

    @classmethod
    def from_query_result(cls,id, text, topic_id, user_email, best_reply, upvotes, downvotes):
        return cls(
                id = id,
                text = text,
                topic_id = topic_id,
                user_email = user_email,
                best_reply = best_reply,
                upvotes = upvotes,
                downvotes = downvotes
                )
   
class BestReply(BaseModel):
    best_reply: bool 

class VoteType(str, Enum):
    upvote = 'upvote'
    downvote = 'downvote'
    
class Vote(BaseModel):
    vote_type: VoteType

class PrivacyType(str, Enum):
    pricate = 'private' # 1
    public = 'public' # 0

class Category(BaseModel):
    name: constr(strip_whitespace=True, min_length=2, max_length=50)
    is_private: PrivacyType | None = 'public'
    is_locked: LockedType | None

    @classmethod
    def from_query_result(cls, name: str, is_private: str, is_locked):
        return cls(name=name,
                   is_private=('public' if is_private == 0 else 'private'),
                   is_locked=  'locked' if is_locked == 1 else 'unlocked')

class Pagination(BaseModel):
    page: conint(ge=1)
    page_size: conint(ge=5)
    total_count: conint(ge=0)
    total_pages: conint(ge=0)

    @classmethod
    def from_query_result(cls, page: int, page_size: int, total_count: int, total_pages: int):
        return cls(
                page=page,
                page_size=page_size,
                total_count=total_count,
                total_pages=total_pages)
    
class CategoryResponseModel(BaseModel):
    categories: list[Category] 
    pagination: Pagination

    @classmethod
    def from_query_result(cls, data: list[Category], args):
        return cls(
            categories = data, 
            pagination = (Pagination.from_query_result(*args)))

class TopicResponseModel(BaseModel):
    topics: list[Topic]
    pagination: Pagination
    @classmethod
    def from_query_result(cls, data: list[Topic], args):
        return cls(
            topics = data, 
            pagination = Pagination.from_query_result(*args))
    
class Message(BaseModel):
    id: conint(ge=1) | None = None
    text:str
    sender: str
    receiver: str
    creation_time: str  | None = None

    @classmethod
    def from_query_result(cls, id, text: str, sender: str, receiver: int, creation_time: datetime.datetime):
        formatted_creation_time = creation_time.strftime('%Y-%m-%d, %H:%M:%S')
        return cls(
            id=id,
            text=text,
            sender=sender,
            receiver=receiver,
            creation_time=formatted_creation_time)
    
class MessageData(BaseModel):
    receiver_email: str
    text: constr(strip_whitespace=True, min_length=2, max_length=4000)
    id: int | None = None
    @classmethod
    def from_query_result(cls, receiver_email: str, text: int, id: int):
        return cls(
                receiver_email=receiver_email,
                text=text,
                id=id)


class Roles(str, Enum):
    user = 'user'  
    admin = 'admin'

class AdminUser(BaseModel):
    """"Class for User validations"""

    username: str
    email: str
    password: str
    role: Roles

    @classmethod
    def from_query_result(cls,email, password, username,role):
        return cls(
            
            email=email,
            password=password,
            username=username,
            role = ('admin' if role == 1 else 'user')
        )
    

class AccessType(str, Enum):
    restricted = 'restricted'
    read_only = 'read_only'
    full = 'full'

class User(BaseModel):
    """"Class for User validations"""
    username: str
    email: str
    password: str

    @classmethod
    def from_query_result(cls,email, password, username):
        return cls( 
            email=email,
            password=password,
            username=username)
        
class UserOut(BaseModel):
    username: str
    email: str
    role: Roles

    @classmethod
    def from_query_result(cls,email,username, role):
        return cls(
            email=email,
            username=username,
            role = ('admin' if role == 1 else 'user'))
    
class UserLogin(BaseModel):
    email: EmailStr
    password: str

    @classmethod
    def from_query_result(cls,email,password):
        return cls(
            
            email=email,
            password=password,
        )
    

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None
    role: Roles


class UserAccessForCategory(BaseModel):
    email: EmailStr
    category: str
    access: AccessType

    @classmethod
    def from_query_result(cls,email,category,access):
        return cls(
            email=email,
            category=category,
            access=access)
    
class UserCategoryAccessOut(BaseModel):
    
    email: str
    access: AccessType

    @classmethod
    def from_query_result(cls, email, access):

        return cls(
            email=email,
            access=('full' if access == 3 else ('read_only' if access == 2 else 'restricted'))
        )