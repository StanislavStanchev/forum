import services.users_service
from pydantic import EmailStr
import services.topics_service
from fastapi import APIRouter, Depends, Response
from services import replies_service, categories_service
from common.responses import NotFound, Forbidden, Conflict, NoContent
from data.models import CreateReply, ReplyResponseModel, UserOut


reply_router = APIRouter(prefix='/replies', tags=['Replies'])


@reply_router.get('/', response_model=list[ReplyResponseModel])
def get_replies(search: str | None = None, email_for_filtering: EmailStr | None = None,
                current_user: UserOut = Depends(services.users_service.get_current_user)):    
    """
    ## Get Replies
    Returns all replies.
    ### Query Parameters
    - `search` (str, optional): A search string to filter replies by.
    - `email` (EmailStr, optional): An email address to filter replies by.
    ### Response:
    Returns a list of JSON objects representing the replies.
    Example:
    ```
    [
        {
            "id": 1,
            "message": "Thanks for reaching out!",
            "email": "john.doe@example.com"
        }
    ]
    ```
    """
 
    return replies_service.all(current_user, search, email_for_filtering=email_for_filtering)


@reply_router.get('/{id}', response_model=ReplyResponseModel)
def get_reply_by_id(id: int, current_user: UserOut = Depends(services.users_service.get_current_user)):
    """
    ## Returns a specific Reply by ID.
    ### Args:
    - `id` (int): The ID of the Reply.
    ### Response:
    - `NotFound`: If no Reply with the specified ID is found.
    - Returns JSON object representing `ReplyResponseModel`: The Reply with the specified ID.
    Example:
    ```
    {
        "id": 1,
        "text": "What a great topic!",
        "topic_id": 2,
        "user_email": "john.doe@example.com",
        "best_reply": true
    }
    ```
    """

    reply = replies_service.get_by_id(id)

    if not reply:
        return NotFound(content=f'Reply with id No. {id} does not exist.')
    
    category_access = categories_service.validate_category_access(current_user, reply_id=reply.id)

    if isinstance(category_access, Forbidden):
        return category_access
    
    return reply


@reply_router.post('/', status_code= 201, response_model=ReplyResponseModel)
def create_reply(reply: CreateReply, current_user: UserOut = Depends(services.users_service.get_current_user)):
    """
    ## Creates and returns a new Reply. Authentication required.
    ### Args:
    - `reply` (`CreateReply`): Text and id of the topic is required.
    - `current_user` (`UserOut`, optional): The authenticated user. Defaults to Depends(services.users_service.get_current_user).
    ### Response:
    - `NotFound`: If the specified topic does not exist.
    - Returns JSON object representing `ReplyResponseModel`: The newly created Reply with id, text, topic_id, email of logged user and default false flag for best reply.
    Example:
    ```
    {
        "id": 1,
        "text": "What a great topic!",
        "topic_id": 2,
        "user_email": "john.doe@example.com",
        "best_reply": false
    }
    ```
    """
    if services.topics_service.topic_is_locked(reply.topic_id):
        return Conflict(content=f'Topic with id {reply.topic_id} is locked.')
    
    if not services.topics_service.topic_exists(reply.topic_id):   
        return NotFound(content=f'Topic {reply.topic_id} does not exist.')

    category_access = categories_service.validate_category_access(current_user, topic_id=reply.topic_id, required_access=('full'))
    
    if isinstance(category_access, Forbidden):
        return category_access
    
    return replies_service.create(reply, current_user)
  

@reply_router.delete('/{id}', status_code=204)
def delete_reply(id: int, current_user: UserOut = Depends(services.users_service.get_current_user)):
    """
    ## Deletes a Reply with the specified ID. Only the author of the Reply can delete it.
    ### Args:
    - `id` (int): The ID of the Reply to be deleted.
    - `current_user` (`UserOut`, optional): The authenticated user. Defaults to Depends(services.users_service.get_current_user).
    ### Response:
    - `NotFound`: If no Reply with the specified ID is found.
    - `Forbidden`: If the authenticated user is not the author of the Reply.
    - If  successful, returns None (code HTTP code 204 No Content).
    """
    
    reply = replies_service.get_by_id(id)

    if not reply:
        return NotFound(content=f'Reply with id No. {id} does not exist.')

    if reply.user_email != current_user.email:
        return Forbidden(content="Not authorized. Only author can delete replies.")
    
    category_access = categories_service.validate_category_access(current_user, topic_id=reply.topic_id, required_access=('full'))
    
    if isinstance(category_access, Forbidden):
        return category_access
    
    replies_service.delete(id)

    return NoContent()