from fastapi import APIRouter
from fastapi.responses import FileResponse

home_router = APIRouter(prefix="",tags=['Home'])

@home_router.get("/")
async def home():
    return FileResponse("common/home.html")