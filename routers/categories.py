
from pydantic import constr, conint
from fastapi import APIRouter, Path, Depends
from data.models import Category, LockedType, UserOut
from services import categories_service, users_service
from common.responses import NotFound, Conflict, Forbidden


category_router = APIRouter(prefix='/categories', tags=['Categories'])

@category_router.get('/')
def get_categories(search: constr(strip_whitespace=True, max_length=100) | None = None,
                sort: constr(strip_whitespace=True, min_length=3, max_length=4) | None = None,
                page: conint(ge=1) = 1,
                page_size: conint(ge=5) = 5,
                current_user: UserOut = Depends(users_service.get_current_user)):
    """
    ## Returns a list of all Categories.
    ### Args:
    - `search` (`str`, optional): Search term to filter Categories by name.
    - `sort` (`str`, optional): Sorting order by name. The value must be either `asc` or `desc`.
    - `page` (`int`, optional): The page number to return. The default is `1`.
    - `page_size` (`int`, optional): The number of Categories to return per page. The default is `5`.
    ### Response:
    - Returns a JSON object representing a list of all Categories:
    ``` 
        {
            "categories": [
                {
                    "name": "Databases",
                    "is_private": "private",
                    "is_locked": "locked"
                }
            ]
        }
    ```
    """

    return categories_service.all(current_user, search, sort, page, page_size)


@category_router.get('/{name}')
def get_category(name: str = Path(..., regex="^[a-zA-Z0-9_-]+$"),
                search: constr(strip_whitespace=True, min_length=1, max_length=50) | None = None,
                sort: constr(strip_whitespace=True, min_length=3, max_length=4) | None = None,
                sort_by: constr(strip_whitespace=True) | None = None, 
                page: conint(ge=1) = 1,
                page_size: conint(ge=5) = 5,
                current_user: UserOut = Depends(users_service.get_current_user)):
    """
    ## Returns a list of all Topics that belong to a Category with a specified name.
    ### Args:
    - `name` (`str`): The name of the Category.
    - `search` (`str`, optional): Search term to filter Topics by title.
    - `sort` (`str`, optional): Sorting order by the number of replies. The value must be either `asc` or `desc`.
    - `sort_by` (`str`, optional): Sorting criteria. The value can be one of `replies_count` or `created_at`.
    - `page` (`int`, optional): The page number to return. The default is `1`.
    - `page_size` (`int`, optional): The number of Topics to return per page. The default is `5`.
    ### Response:
    - `NotFound`: If a Category with the specified name does not exist.
    - Returns a tuple consisting of:
      - A JSON object representing the Category and list of all Topics that belong to the Category:
        ```
        {
            "name": "Databases",
            "is_private": "private",
            "is_locked": "locked"
        }
        {
            "topics": [
                {
                    "id": 21,
                    "text": "Hey",
                    "user_email": "stanislav@gmail.com",
                    "category_name": "New example topic to check best reply",
                    "title": "Databases",
                    "is_locked": "unlocked"
                }
            ]
        ```
    """
    
    name = name.replace('-', ' ')
    category = categories_service.get_by_name(name)

    if not category:
        return NotFound()
    
    category_access = categories_service.validate_category_access(current_user, category_name=name)
    
    if isinstance(category_access, Forbidden):
        return category_access

    paginated_topics_in_category = categories_service.get_topics_in_category(name, search, sort, sort_by, page, page_size) 

    return category, paginated_topics_in_category


@category_router.post('/',status_code=201)
def post_categories(category:Category,_ = Depends(users_service.validate_admin)):
    current_category = categories_service.exists(category.name)
    if current_category:
        return Conflict(content="Category already exist!")

    return categories_service.create_category(category=category)


@category_router.patch('/',status_code=201)
def update_category_privacy(category:Category,_ = Depends(users_service.validate_admin)):
    
    return categories_service.patch_category(category=category)


@category_router.patch('/{category_name}', status_code=201)
def lock_category(category_name: str, is_locked: LockedType, _=Depends(users_service.validate_admin)):
    
    category = categories_service.get_by_name(category_name)
    if not category:
        return NotFound(content=f'Category with name {category_name} does not exist')
    
    if is_locked == 'locked' and category.is_locked == 'locked': 
        return Conflict(content='Category already locked!')
    
    if is_locked == 'unlocked' and category.is_locked == 'unlocked': 
        return Conflict(content='Category already unlocked!')
    
    return categories_service.lock(category, is_locked)