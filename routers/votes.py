import services.users_service
from data.models import VoteType, UserOut
from fastapi import APIRouter, status, Depends
from common.responses import NotFound, Forbidden
from services import votes_service, replies_service, categories_service


vote_router = APIRouter(prefix='/replies', tags=['Votes'])

@vote_router.put('/{id}/votes', status_code=status.HTTP_201_CREATED)
def vote_on_reply(id: int, vote: VoteType,
                current_user: UserOut = Depends(services.users_service.get_current_user)):
    """
    Logged User can upvote or downvote a Reply only once. The vote can be changed.

    ### Args:
    - `id` (`int`): The ID of the reply to vote on.
    - `vote` (`VoteType`): The type of vote (upvote or downvote).

    ### Response:
    - Returns a response with status code 201 if the vote is successfully recorded.
    - `NotFound`: If the reply with the specified ID does not exist.
    - `Forbidden`: If the current user does not have full access to the category the reply belongs to.
    """

    if not replies_service.exists(id):
        return NotFound(content=f'Reply with id No. {id} does not exist.')
    
    vote = votes_service.vote(id, vote, current_user.email)    
    reply = replies_service.get_by_id(id)
    
    category_access = categories_service.validate_category_access(current_user, reply_id=id, required_access=('full'))
    
    if isinstance(category_access, Forbidden):
        return category_access

    return vote, reply