
from fastapi import APIRouter, Depends,Query
from data.models import User,Token,AdminUser, EmailStr, AccessType
from services import users_service,categories_service
from fastapi.security import OAuth2PasswordRequestForm
from common.responses import Forbidden,NotFound,Conflict
from typing import Optional

users_router = APIRouter(prefix='/users',tags=['Users'])



@users_router.get('/{email}')
def user_by_email(email: str,user_auth:str = Depends(users_service.get_current_user)):
    """
    ## user_by_email
    Return current used with providen email, return NotFound 404 if it does not exist.
    ### Query Parameters
    - `search` (str, optional): A search string to filter replies by.
    - `email` (EmailStr): An email address to filter users by.
    ### Response
    Returns a  JSON object representing the users.
    Example:
    ```
    {
            "username": "313141234141",
            "email": "maimun2@abv.bg"
    }
    ```
    """
    user= users_service.get_by_email(email=email)

    if user:
        return user
    return NotFound(content=f"Used with email:{email}, does not exist!")


@users_router.get('/',status_code=200,)
def get_users(
    sort: str | None = None,
    search: str | None = None,
    category_name: Optional[str] = Query(None, regex="^[a-zA-Z0-9_-]+$"),_ = Depends(users_service.validate_admin)
):  
    """
    ## get_users
    Return all registered users
    ### Query Parameters
    
    
    ### Response
    Returns a  JSON object representing the users.
    Example:
    ```
    {
            "username": "313141234141",
            "email": "maimun2@abv.bg"
    }
    ```
    """
    if category_name:
        
        category_name = category_name.replace('-', ' ')
        exist=categories_service.exists(category_name)
        if not exist:
            return NotFound("There is no such category!")
        is_private = categories_service.is_private(category_name)
        if not is_private:
            return Conflict("This category is public!")
        search = category_name
    result = users_service.all(search)

    if sort and (sort == 'asc' or sort == 'desc'):
        return users_service.sort(result, reverse=sort == 'desc')
    else:
        return result

@users_router.post('/', status_code=201)
def create_user(user: User):
    """
    ## create_user
    validating credentials and return message if everything is okay 
    ### Query Parameters
    
    
    ### Response
    Returns a  message .
    Example:
    ```
    "Your username:Petoaaa, email:petyo_denev2222222@abv.bg You can now login."
    """

    return users_service.create(user=user)


@users_router.post('/login',status_code=201, response_model=Token)
def login_verify(user: OAuth2PasswordRequestForm = Depends()):
    """
    ## login_verify
    This function check if the providen password matches the user password in our DB, if it is true we are returning current user and providing him JWT access tokken 
    ### Query Parameters
    
    
    ### Response
    Returns a  JWT token .
    Example:
    ```
    {
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFidi5iZyIsInJvbGUiOiJhZG1pbiIsImV4cCI6MTY4NDA2MDQ0Mn0.2-5hpT7Yk6CRFPRYvsAsDEsfpZ50jyTzhlAzkyiqda4",
                    
    "token_type": "bearer"
    }
    """
    stored_password = users_service.get_stored_password(user.username)  

    if not stored_password:     
        return Forbidden(content=f"Invalid credentials")

    result = users_service.validate_passwords(user.password, stored_password[0][0]) 
    if not result:
        return NotFound(content="Invalid Credentials.")
    else:
        access_token = users_service.create_access_token(data= {"email":user.username, 
                                                                "role":users_service.get_current_role(user.username)})
        return {"access_token": access_token,"token_type":"bearer"}
    

@users_router.post('/admins', status_code=201)
def create_admin_user(admin_user: AdminUser, _=Depends(users_service.validate_admin)):
    """
    ## admin_user
    validating credentials and return message if everything is okay 
    ### Query Parameters
    
    
    ### Response
    Returns a  message .
    Example:
    ```
    "Your username:Petoaaa, email:petyo_denev2222222@abv.bg You can now login."
    """

    return users_service.admin_user_create(admin_user)


@users_router.patch('/rights', status_code=201)
def update_category_member_access(user_email: EmailStr, access_type: AccessType, 
                                    category_name: str = Query(..., regex="^[a-zA-Z0-9_-]+$"), 
                                    _ = Depends(users_service.validate_admin)):

    """Update category member access. Admin authentication required.

    ### Args:
    - `user_email` (`str`): Email of the user whose access is being updated.
    - `access_type` (`AccessType`): Type of access to grant to the user.
    - `category_name` (`str`, optional): Name of the category to update access for(use hyphen instead of space).

    ### Response:
    - Returns a response with status code 201 if the update is successful.
    - `NotFound`: If the user or category does not exist.
    - `Conflict`: If the user is an admin and the access type is not 'full'.
    """

    user = users_service.get_by_email(email=user_email)

    if not user:
        return NotFound(content=f"User with email:{user_email} does not exist.")
    
    category_name = category_name.replace('-', ' ')
    category = categories_service.get_by_name(category_name)

    if not category:
        return NotFound(content=f"Category with name: {category_name} does not exist.")

    if user.role == 'admin' and access_type != 'full':
        return Conflict(content="The user is Admin. Admin's access should be always 'full'.")
    
    return categories_service.update_user_access(user, access_type, category=category)