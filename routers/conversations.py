from fastapi import APIRouter, Depends, status, Query
from services import users_service, conversations_service
from data import models
from common.responses import NotFound, NoContent
from fastapi.responses import JSONResponse

conversation_router = APIRouter(prefix='/conversations', tags=['Conversation'])

@conversation_router.post('/', status_code = status.HTTP_201_CREATED, response_model = models.Message)
def create_conversation(data: models.MessageData,
                        current_user: models.UserOut = Depends(users_service.get_current_user)):
    receiver = users_service.get_by_email(data.receiver_email)
    if receiver:
        message = conversations_service.create(current_user.email, receiver.email, data.text)
        return message
    return NotFound(content=f'User with email {data.receiver_email} does not exist.')

        
@conversation_router.get('/users')
def get_user_conversations(current_user: models.UserOut = Depends(users_service.get_current_user)):
    all_messages_receivers = conversations_service.get_conversations(current_user.email)
    if not all_messages_receivers:
        return NotFound(content=f'User with email {current_user.email} has no conversations.')
    return JSONResponse(content={'users': all_messages_receivers})

@conversation_router.get('/')
def get_conversations_content(receiver_email: str = Query(...), current_user: models.UserOut = Depends(users_service.get_current_user)):
    receiver = users_service.get_by_email(receiver_email)
    if not receiver:
        return NotFound(content=f'User with email {receiver_email} does not exist.')
    all_conversations_content = conversations_service.get_conversations_text(current_user.email, receiver_email)
    if not all_conversations_content:
        return NotFound(content=f'User with email {current_user.email} has no conversations.')
    return all_conversations_content

@conversation_router.put('/')
def update_conversation_text(data: models.MessageData,
                        current_user: models.UserOut = Depends(users_service.get_current_user)):
    receiver = users_service.get_by_email(data.receiver_email)
    message = conversations_service.get_by_id(data.id)
    if not message:
        return NotFound(content=f'Message with id {data.id} does not exist.')
    if receiver:
        data = conversations_service.update_text(data, current_user.email)
        return data
    return NotFound(content=f'User with email {data.receiver_email} does not exist.')

@conversation_router.delete('/', status_code=204)
def update_conversation_text(data: models.MessageData,
                        current_user: models.UserOut = Depends(users_service.get_current_user)):
    receiver = users_service.get_by_email(data.receiver_email)
    message = conversations_service.get_by_id(data.id)
    if not message:
        return NotFound(content=f'Message with id {data.id} does not exist.')
    if receiver:
        data = conversations_service.delete_message(data, current_user.email)
        if data:
         return NoContent()
    return NotFound(content=f'User with email {data.receiver_email} does not exist.')
