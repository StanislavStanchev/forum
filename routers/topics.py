from fastapi import APIRouter, Query, Depends
from pydantic import conint
from data.models import Topic, ReplyResponseModel, BestReply, UserOut, LockedType
from services import topics_service, users_service
from common.responses import Forbidden, Conflict
from services import categories_service
from common.responses import NotFound
from services import replies_service
from common import utils



topic_router = APIRouter(prefix='/topics', tags=['Topics'])

@topic_router.post('/', status_code=201)
def create_topic(topic: Topic, current_user: UserOut = Depends(users_service.get_current_user)):
    if categories_service.category_is_locked(topic.category_name):
        return Conflict(content='Category is locked!')
    if not categories_service.exists(topic.category_name):
        return NotFound(content=f"Category '{topic.category_name}' does not exist")
    
    category_access = categories_service.validate_category_access(current_user,category_name=topic.category_name)
    
    if isinstance(category_access, Forbidden):
        return category_access
    
    return topics_service.create(topic, current_user)

@topic_router.get('/')
def get_topics( sort: str | None = None, search: str | None = Query(None,regex="^[a-zA-Z0-9_-]+$"), 
               sort_by: str | None = None, page: conint(ge=1) = 1, items_per_page: conint(ge=5) = 5,
               current_user: UserOut = Depends(users_service.get_current_user)):
    if search:
        search = search.replace('-', ' ')
    all_topics = topics_service.wanted_topics(sort=sort, search=search, sort_by=sort_by, current_user=current_user)
    paginated_topics = utils.paginate(all_topics, page, items_per_page)
    return paginated_topics


@topic_router.get('/{id}')
def get_by_id(id: int, current_user: UserOut = Depends(users_service.get_current_user)):
    topic = topics_service.get_topic_by_id(id)
    replies = replies_service.get_by_topic_id(id)
    if not topic:
        return NotFound(content=f'Topic with id: {id} does not exist')
    
    category_access = categories_service.validate_category_access(current_user, topic_id=topic.id)
    
    if isinstance(category_access, Forbidden):
        return category_access
    
    return topic, replies


@topic_router.put('/{topic_id}/replies/{reply_id}', response_model = ReplyResponseModel)
def choose_best_reply(topic_id: int, reply_id: int,                      
                      best_reply_flag: BestReply, current_user: UserOut = Depends(users_service.get_current_user)):
    '''Topic Author can select one best reply to their Topic. Auth required.'''

    topic: Topic = topics_service.get_topic_by_id(topic_id)
    if not topic:
        return NotFound(content=f'Topic {topic_id} does not exist')
    
    reply = replies_service.get_by_id(reply_id) 
    if not reply:
        return NotFound(content=f'Topic {topic_id} does not exist')

    category_access = categories_service.validate_category_access(current_user, topic_id=topic.id, required_access='full')
    
    if isinstance(category_access, Forbidden):
        return category_access

    if topic.user_email != current_user.email:
        return Forbidden(content="Not authorized. Topic Author can select one best reply")

    return replies_service.choose_best_reply(reply, best_reply_flag)

@topic_router.patch('/{id}', status_code=201)
def lock_topic(id: int, is_locked: LockedType, _=Depends(users_service.validate_admin)):
    
    topic = topics_service.get_topic_by_id(id)
    if not topic:
        return NotFound(content=f'Topic with id {id} does not exist')
    
    if is_locked == 'locked' and topic.is_locked == 'locked': 
        return Conflict(content='Topic already locked!')
    
    if is_locked == 'unlocked' and topic.is_locked == 'unlocked': 
        return Conflict(content='Topic already unlocked!')
    
    return topics_service.lock(topic, is_locked)
    