from data.models import TopicResponseModel, Category, CategoryResponseModel

def get_offset(page, page_size):
    return (page - 1) * page_size

def get_total_pages(total_count, page_size):
    return (total_count+ page_size - 1) // page_size

def paginate(items: list, page: int, page_size: int):
    total_count = len(items)
    total_pages = get_total_pages(total_count, page_size)
    start_index = get_offset(page, page_size)
    end_index = start_index + page_size
    data = items[start_index:end_index]

    if data == []:
        return []

    if all(isinstance(item, Category) for item in items):
        return CategoryResponseModel.from_query_result(data, (page, page_size, total_count, total_pages))

    return TopicResponseModel.from_query_result(data, (page, page_size, total_count, total_pages))
