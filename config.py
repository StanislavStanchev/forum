from pydantic import BaseSettings

class Settings(BaseSettings):
    database_hostname: str
    database_port: int
    database_password: str
    database_name: str
    database_username: str
    
    class Config:
      env_file = ".env"

settings = Settings()

class SettingsToken(BaseSettings):
    secret_key: str
    algorithm: str
    access_token_expire_minites: int

    class Config:
      env_file = ".env"

settings_token = SettingsToken()