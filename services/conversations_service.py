from data.database import read_query, insert_query, update_query
from data.models import Message, MessageData


def get_by_id(id: int, get_data_func = None) -> Message | None:
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('SELECT * FROM messages WHERE id = ?', (id,))

    return next((Message.from_query_result(*row) for row in data), None)

def create(current_user_email: str, receiver_email: str, text: str) -> Message:
    generated_id = insert_query('INSERT INTO messages(text,sender,receiver) VALUES(?,?,?)',
        (text, current_user_email, receiver_email))
    
    return get_by_id(generated_id)

def get_conversations(user_email: str) -> list[str]:
    data = read_query(
            '''SELECT DISTINCT receiver
               FROM messages 
               WHERE sender LIKE ?''', (f'%{user_email}%',))
    users_receivers = []
    for row in data:
        users_receivers.append(*row)
    
    return users_receivers

def get_conversations_text(user_email: str, receiver_email: str) -> list[MessageData]:
    data = read_query(
            '''SELECT receiver, text, id
               FROM messages 
               WHERE sender LIKE ? and receiver LIKE ?''', (f'%{user_email}%', f'%{receiver_email}%'))

    return list(MessageData.from_query_result(*row) for row in data)

def update_text(data: MessageData, user_email: str) -> MessageData:
    update_query('''UPDATE messages SET text = ? WHERE receiver = ? and id = ?
                        and sender =  ? ''', (data.text, data.receiver_email, data.id, user_email))
    return data
    

def delete_message(data: MessageData, user_email: str) -> MessageData:
    data = update_query('''DELETE FROM messages WHERE receiver = ? and id = ?
                        and sender =  ? ''', (data.receiver_email, data.id, user_email))
    return data