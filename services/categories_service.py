import common.utils
from pydantic import conint
from pydantic import EmailStr
import services.users_service
import services.topics_service
from common.responses import Forbidden,Conflict, NotFound
from data.database import read_query, update_query,insert_query
from data.models import CategoryResponseModel, Category, Topic, UserOut, AccessType, UserAccessForCategory


def is_private(category_name: str, get_data_func = None) -> bool:
    if get_data_func is None:
        get_data_func = read_query

    return any(get_data_func('''SELECT is_private FROM categories 
                             WHERE name = ? AND is_private = 1''', (category_name,)))


def is_category_member(user: UserOut | None = None, user_email: EmailStr | None = None,
                       category_name: str | None = None, category: Category | None = None, 
                       get_data_func = None) -> bool:
    if get_data_func is None:
        get_data_func = read_query

    email = user.email if user and user.email else user_email
    category_name =  category.name if category and category.name else category_name


    return any(get_data_func('''SELECT user_email FROM users_has_categories 
                             WHERE user_email = ? AND category_name = ? AND access in (1,2,3)''', (email, category_name)))


def exists(name: str, get_data_func = None) -> bool:
    if get_data_func is None:
        get_data_func = read_query

    return any(get_data_func('SELECT name FROM categories WHERE name = ?', (name,)))


def sort_categories(lst: list[Category], reverse=False):
    return sorted(lst, key=lambda c: c.name, reverse=reverse)

def get_by_name(name: str, get_data_func = None) -> Category:
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('SELECT * FROM categories WHERE name = ?', (name,))

    return next((Category.from_query_result(*row) for row in data), None)

def all(current_user: UserOut,
        search: str | None = None,
        sort: str | None = None,
        page: conint(ge=1) = 1,
        page_size: conint(ge=5) = 5,
        get_data_func = None) -> CategoryResponseModel:
    '''Responds with a list of all Categories'''
    if get_data_func is None:
        get_data_func = read_query

    # Selects public topics only and where the logged user is category member with full or read_only access.
    sql = '''SELECT c.name, c.is_private, c.is_locked 
        FROM categories AS c 
        LEFT JOIN users_has_categories AS uhc ON c.name = uhc.category_name 
        WHERE (c.is_private = 0)
        OR (c.is_private = 1 AND uhc.access in (2,3) AND uhc.user_email = ?)'''
    
    where_clauses = []

    if search:
        sql += ' AND c.name LIKE ?'
        where_clauses.append(f'%{search}%')
    sql += ' GROUP BY c.name'

    data = get_data_func(sql, (current_user.email, *where_clauses))

    data = list(Category.from_query_result(*row) for row in data)
    
    if sort and (sort == 'asc' or sort == 'desc'):
        data = sort_categories(data, reverse=sort == 'desc')

    paginated_categories = common.utils.paginate(data, page, page_size)
    
    return paginated_categories


def get_topics_in_category(name: Category, search: str,
                        sort: str, sort_by: str,
                        page: int, page_size: int,
                        get_data_func = None):
    '''Responds with a list of all Topics that belong to that Category'''
    if get_data_func is None:
        get_data_func = read_query

    if search:
        data = get_data_func('''SELECT * FROM topics
                        WHERE category_name = ? AND text LIKE ?''', (name, f'%{search}%'))
    else:
        data = get_data_func('''SELECT * FROM topics
                        WHERE category_name = ?''', (name,))
        
    data = list(Topic.from_query_result(*row) for row in data)
    
    if sort and (sort == 'asc' or sort == 'desc'):
        data = services.topics_service.sort_topics(data, attribute=sort_by, reverse=sort == 'desc')
     
    paginated_topics = common.utils.paginate(data, page, page_size)

    return paginated_topics


def update_user_access(user: UserOut, access_type: AccessType, category: Category, 
                       update_data_func = None, insert_data_func = None) -> UserOut:
    if update_data_func is None:
        update_data_func = update_query
    if insert_data_func is None:
        insert_data_func = insert_query

    is_member = is_category_member(user=user, category=category)
    if is_member:
        update_data_func('UPDATE users_has_categories SET access = ? WHERE user_email = ? AND category_name = ?',
                            (_convert_access_to_db(access_type) , user.email, category.name))
    else: 
        insert_data_func('INSERT INTO users_has_categories(access,user_email,category_name) VALUES (?,?,?)',
            (_convert_access_to_db(access_type), user.email, category.name))

    return (UserAccessForCategory.from_query_result(user.email, category.name, access_type))


def get_user_access_for_category(email: EmailStr, category: Category = None,
                                 category_name: str = None, get_data_func = None)-> str:
    if get_data_func is None:
        get_data_func = read_query
    
    category_name = category_name if category_name else category.name

    access = get_data_func('''SELECT access FROM users_has_categories
                                WHERE user_email = ? AND category_name = ?''', (email,category_name))

    if not access:
        return Forbidden(f'You are not a category member of "{category_name}".')

    return ('full' if access[0][0] == 3 else('read_only' if access[0][0] == 2 else 'restricted'))


def validate_category_access(user: UserOut, category_name: str = None, 
                                topic_id: int = None, reply_id: int = None,
                                required_access = ('full', 'read_only'), get_data_func = None):

    if get_data_func is None:
        get_data_func = read_query

    if category_name and not is_private(category_name):
        return (True, category_name)

    if topic_id: 
        category_from_query = get_data_func('SELECT category_name FROM topics WHERE id = ?', (topic_id,))
    
    if reply_id:
        category_from_query = get_data_func('''SELECT c.name FROM categories AS c
                                JOIN topics AS t ON c.name = t.category_name
                                JOIN replies AS r ON t.id = r.topic_id
                                WHERE r.id = ? ''', (reply_id,))
    
    category_name = (category_name if category_name else category_from_query[0][0])

    user_access = get_user_access_for_category(email=user.email, category_name=category_name)
   
    if user_access not in required_access:
        return Forbidden(content=f'Restricted access to category "{category_name}"')
    else: 
        return (True, category_name)
    

def _convert_access_to_db(access):
    return (3 if access == 'full' else(2 if access == 'read_only' else 1))


def create_category(category: Category):
    insert_query('INSERT INTO categories(name, is_private) VALUES (?, ?)', (category.name, 1 if category.is_private=="private" else 0))
    return f"Category with name: {category.name}, was created as {category.is_private}"


def patch_category(category:Category):
    
    current_category = read_query('SELECT * FROM categories WHERE name = ?', (category.name,))
    if not current_category:
        return NotFound(content="Category does not exist!")
    if category.is_private == "private":
        category.is_private = 1
    else:
        category.is_private = 0
    if current_category[0][1] == category.is_private:
        return Conflict(content="You can not with same private level!")
 
    update_query('UPDATE categories SET is_private = ? WHERE name = ?', (category.is_private, category.name))
    
    if category.is_private is None:
        return f"No update needed for category '{category.name}'"
    else:
        is_private_str = 'private' if category.is_private else 'public'
        return f"Category '{category.name}' was updated as: {is_private_str}"


def lock(category: Category, is_locked: str):

    update_query('''UPDATE categories SET is_locked = ?
                    WHERE name = ?''',
                 (1 if is_locked == 'locked' else 0, category.name))
    
    
    category.is_locked = is_locked
    
    return category 
    
def category_is_locked(name: str) -> bool:
    return any(
    read_query(
            'select  name from categories where name = ? and is_locked = ?',
            (name, 1)))