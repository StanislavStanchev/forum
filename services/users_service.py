from fastapi import Depends, HTTPException, status
from jose import jwt,JWTError
from config import settings_token
from datetime import datetime,timedelta
from passlib.context import CryptContext
from pydantic import ValidationError, EmailStr
from fastapi.security import OAuth2PasswordBearer
from data.database import insert_query, read_query
from common.responses import Unprocessable
from data.models import User, UserOut, TokenData, AdminUser,UserCategoryAccessOut


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/users/login")


pwd_content = CryptContext(schemes=["bcrypt"],deprecated="auto")


def is_email_in_use(email: EmailStr, get_data_func = None)-> bool:
    if get_data_func is None:
        get_data_func = read_query

    return any(get_data_func('SELECT email FROM users WHERE email = ?', (email,)))


def is_username_in_use(username: str, get_data_func = None)-> bool:
    if get_data_func is None:
        get_data_func = read_query

    return any(get_data_func('SELECT username FROM users WHERE username = ?', (username,)))


def get_by_email(email: str, get_data_func = None) -> UserOut | None:
    if get_data_func is None:
        get_data_func = read_query
    
    data = get_data_func('SELECT email,username,role FROM users WHERE email = ?', (email,))

    return next((UserOut.from_query_result(*row) for row in data), None)


def get_stored_password(email:str, get_data_func = None)-> str | None:
    if get_data_func is None:
        get_data_func = read_query

    return get_data_func('SELECT password FROM users WHERE email = ?', (email,)) 


def get_current_role(email:str, get_data_func = None)-> str | None:
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('SELECT role FROM users WHERE email = ?', (email,))
    return  ('admin' if data[0][0] == 1 else 'user')


def get_current_user(token:str = Depends(oauth2_scheme)) -> UserOut | None:
   
    token: TokenData = verify_access_token(token) 

    return get_by_email(token.email)  


def all(search: str = None, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    if search is None:
        data = get_data_func('SELECT email, username, role FROM users')
        return (UserOut.from_query_result(*row) for row in data)
    elif search:
        data = get_data_func('SELECT user_email, access FROM users_has_categories WHERE category_name = ?', (search,))
        return (UserCategoryAccessOut.from_query_result(*row) for row in data)
    


def sort(lst: list[User], reverse=False):
    return sorted(lst, key=lambda u: u.email, reverse=reverse)


def create_access_token(data:dict):
    
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=settings_token.access_token_expire_minites)
    to_encode.update({"exp":expire})

    encoded_jwt = jwt.encode(to_encode,settings_token.secret_key,settings_token.algorithm)  
    return encoded_jwt


def create(user: User):
    email = is_email_in_use(user.email)
    username = is_username_in_use(user.username,) 
    if email and username:
        return Unprocessable(content="Username and email already in use !")
    elif email:
        return Unprocessable(content="Email already in use!")
    elif username:
        return Unprocessable(content="Username already in use!")
       
    hashed_password = pwd_content.hash(user.password)
    user.password = hashed_password
    insert_query('INSERT INTO users(email,password,username,role) VALUES(?,?,?,?)', 
            (user.email,user.password,user.username,0))
    return f"Your username:{user.username}, email:{user.email}. You can now login."


def admin_user_create(admin:AdminUser ):
    
    email = is_email_in_use(admin.email)
    username = is_username_in_use(admin.username) 
    if email and username:
        return Unprocessable(content="Username and email already in use !")
    elif email:
        return Unprocessable(content="Email already in use!")
    elif username:
        return Unprocessable(content="Username already in use!")
       
    hashed_password = pwd_content.hash(admin.password)
    admin.password = hashed_password
    insert_query('INSERT INTO users(email,password,username,role) VALUES(?,?,?,?)', 
            (admin.email,admin.password,admin.username, 1))
    return f"Your username:{admin.username}, email:{admin.email} You can now login."


def verify_access_token(token:str):
    
    try:
        payload = jwt.decode(token, settings_token.secret_key,settings_token.algorithm)  
        email: str = payload.get("email")
        token_role = payload.get("role", 'Bearer')
        authenticate_value = f'Bearer scope="{token_role}"'

        credentials_exception = HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail= f"Could not validate credentials",
                headers={"WWW-Authenticate": f"{authenticate_value}"})

        if not email:
            raise credentials_exception

        token_data = TokenData(email=email, role=token_role)
    except (JWTError, ValidationError):
       raise credentials_exception
    
    return token_data


def validate_passwords(given_pass, stored_pass):
    return pwd_content.verify(given_pass, stored_pass)


def validate_admin(token:str = Depends(oauth2_scheme)):
    
    token: TokenData = verify_access_token(token) 
    if token.role != 'admin':
        raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail= f"Only Admins have permission for this action.",
                headers={"WWW-Authenticate": f'Bearer scope={token.role}'})
        
    return get_by_email(token.email)  