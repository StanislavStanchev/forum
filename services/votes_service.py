from data.models import Vote
from pydantic import EmailStr
from data.database import insert_query, read_query, update_query


def is_vote_existing(id, email: EmailStr, get_data_func = None) -> bool:
    if get_data_func is None:
        get_data_func = read_query

    return any(get_data_func('SELECT * FROM votes WHERE reply_id = ? AND user_email = ?', (id, email)))


def vote(id: int, vote: Vote, email: EmailStr, update_data_func = None, insert_data_func = None):
    if update_data_func is None:
        update_data_func = update_query
    if insert_data_func is None:
        insert_data_func = insert_query

    if is_vote_existing(id, email):
        update_data_func('UPDATE votes SET vote = ? WHERE user_email = ? AND reply_id = ?', 
                     (1 if vote == 'upvote' else 0, email, id))
    else:
        insert_data_func('INSERT INTO votes (vote, user_email, reply_id) VALUES (?, ?, ?)',
                      (1 if vote == 'upvote' else 0, email, id))

    return {"msg": ("Upvoted" if vote == "upvote" else "Downvoted")} 

    
def count_votes(id: int, get_data_func = None):
    if get_data_func is None:
        get_data_func = read_query
    
    data = get_data_func('''SELECT 
    COUNT(CASE WHEN vote = 1 THEN 1 END) as upvotes, 
    COUNT(CASE WHEN vote = 0 THEN 1 END) as downvotes 
    FROM votes WHERE reply_id = ?''', (id,))
    
    return data[0][0], data[0][1]