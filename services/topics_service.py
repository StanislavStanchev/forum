from data.models import Topic, UserOut
from data.database import insert_query, read_query, update_query


def get_topic_by_id(id: int, get_data_func = None) -> Topic:
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('SELECT * FROM topics WHERE id = ?', (id,))

    return next((Topic.from_query_result(*row) for row in data), None)


def topic_exists(id: int, get_data_func = None) -> bool:
    if get_data_func is None:
        get_data_func = read_query

    return any(
    get_data_func(
            'select id from topics where id = ?',
            (id,)))

def all(current_user: UserOut, search: str = None, get_data_func = None) -> list[Topic]:
    if get_data_func is None:
        get_data_func = read_query
    # Selects public topics only and where the logged user is category member with full or read_only access.
    sql = '''SELECT t.id, t.text, t.user_email, t.title, t.category_name, t.is_locked
            FROM topics AS t  
            LEFT JOIN categories AS c ON t.category_name = c.name 
            LEFT JOIN users_has_categories AS uhc ON c.name = uhc.category_name 
            WHERE (c.is_private = 0)
            OR (c.is_private = 1 AND uhc.access in (2,3) AND uhc.user_email = ?)''' 

    where_clauses = []

    if search:
        sql += ' AND t.title LIKE ?'
        where_clauses.append(f'%{search}%')
    sql += ' GROUP BY t.id'

    data = get_data_func(sql, (current_user.email, *where_clauses))

    return list(Topic.from_query_result(*row) for row in data)
    
def wanted_topics(
    sort: str ,
    search: str,
    sort_by: str,
    current_user: UserOut,
    get_data_func = None
) -> list[Topic]:  
    result = all(current_user, search, get_data_func)

    if sort and (sort == 'asc' or sort == 'desc'):
        return sort_topics(result, reverse=sort == 'desc', attribute=sort_by)
    else:
        return result

def sort_topics(lst: list[Topic], *, attribute = 'title', reverse=False) -> list[Topic]:
    
    if attribute == 'title':
        def sort_by(topic: Topic): return topic.title
    elif attribute == 'category_name':
        def sort_by(topic: Topic): return topic.category_name
    else:
        def sort_by(topic: Topic): return topic.id
    return sorted(
        lst,
        key=sort_by,
        reverse=reverse)

def create(topic: Topic, current_user: UserOut, insert_data_func = None) -> Topic:
    if insert_data_func == None:
        insert_data_func = insert_query

    generated_id = insert_data_func(
        'INSERT INTO topics(text, user_email, category_name, title) VALUES(?,?,?,?)',
        (topic.text, current_user.email, topic.category_name, topic.title))

    topic.id = generated_id
    topic.user_email = current_user.email

    return topic

def lock(topic: Topic, is_locked: str, update_data_func= None):
    if update_data_func == None:
        update_data_func = update_query

    update_query('''UPDATE topics SET is_locked = ?
                    WHERE id = ?;''',
                 (1 if is_locked == 'locked' else 0, topic.id))
    
    
    topic.is_locked = is_locked
    
    return topic
    
def topic_is_locked(id: int, get_data_func = None) -> bool:
    if get_data_func == None:
        get_data_func = read_query
    return any(
    get_data_func(
            'select id from topics where id = ? and is_locked = ?',
            (id, 1)))
