from services.votes_service import count_votes
from data.database import insert_query, read_query, update_query
from data.models import CreateReply, ReplyResponseModel, BestReply, UserOut


def exists(id: int, get_data_func = None) -> bool:
    if get_data_func is None:
        get_data_func = read_query

    return any(get_data_func('SELECT id FROM replies WHERE id = ?', (id,)))

def get_by_topic_id(id: int, get_data_func = None) -> ReplyResponseModel | None:
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('SELECT * FROM replies WHERE topic_id = ?', (id,))

    return next((ReplyResponseModel.from_query_result(*row, *count_votes(id)) for row in data), None)


def get_by_id(id: int, get_data_func = None) -> ReplyResponseModel | None:
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('SELECT * FROM replies WHERE id = ?', (id,))

    return next((ReplyResponseModel.from_query_result(*row, *count_votes(id)) for row in data), None)


def all(current_logged_user: UserOut, search: str | None = None,
        email_for_filtering: str | None = None, get_data_func = None):
    
    if get_data_func is None:
        get_data_func = read_query

    sql = '''SELECT r.id, r.text, r.topic_id, r.user_email, r.best_reply FROM replies AS r 
            LEFT JOIN topics AS t ON r.topic_id = t.id 
            LEFT JOIN categories AS c ON t.category_name = c.name 
            LEFT JOIN users_has_categories AS uhc ON c.name = uhc.category_name 
            WHERE c.is_private = 0 
            OR (c.is_private = 1 AND access in (2,3) AND uhc.user_email = ?)
            GROUP BY r.id''' # Selects public replies only and where the logged user is category member with full or read_only access.
    
    where_clauses = []

    if search and email_for_filtering: 
        sql += ' AND r.text LIKE ? AND r.user_email = ?'
        where_clauses.extend([f'%{search}%', email_for_filtering])
    elif email_for_filtering:
        sql += ' AND r.user_email = ?'
        where_clauses.append(email_for_filtering)
    elif search:
        sql += ' AND r.text LIKE ?'
        where_clauses.append(f'%{search}%')

    data = get_data_func(sql, (current_logged_user.email, *where_clauses))
    
    return (ReplyResponseModel.from_query_result(*row, *count_votes(row[0])) for row in data)


def create(reply: CreateReply, current_user: UserOut, insert_data_func = None) -> ReplyResponseModel:
    '''Creates and returns Reply.'''
    
    if insert_data_func is None:
        insert_data_func = insert_query

    generated_id = insert_query('INSERT INTO replies(text,topic_id,user_email) VALUES (?,?,?)',
    (reply.text, reply.topic_id, current_user.email))

    reply.id = generated_id
 
    return get_by_id(reply.id)


def choose_best_reply(reply: ReplyResponseModel, best_reply_flag: BestReply, 
                      update_data_func = None) -> ReplyResponseModel:
    """Only the author of the topic could choose best reply."""
    if update_data_func is None:
        update_data_func = update_query

    update_data_func('UPDATE replies SET best_reply = 0 WHERE topic_id = ?', (reply.topic_id,))
    
    best_reply_flag = 1 if best_reply_flag.best_reply == True else 0 

    update_data_func('UPDATE replies SET best_reply = ? WHERE id = ?', (best_reply_flag, reply.id))

    return get_by_id(reply.id)


def delete(reply_id: int, update_data_func = None) -> bool:
    '''Deletes reply from DB based on reply id.'''

    if update_data_func is None:
        update_data_func = update_query

    del_votes = update_data_func('DELETE FROM votes WHERE reply_id = ?',(reply_id,))
    del_replies = update_data_func('DELETE FROM replies WHERE id = ?', (reply_id,))

    return True if del_votes and del_replies else False