![Alt text](images/header.PNG)
***
### Developers: Stanislav Stanchev, Ivan Dimitrov, Petyo Denev (Team 6 in Telerik Alpha 45 Python course)
***
## Summary
This is a Python-based project which aims to implement a Forum System with a RESTful API that allows users to engage in discussions, share messages, and manage various aspects of the forum. The API includes essential endpoints such as user registration and login, reply/topic creation and viewing and message exchange. Additionally, the API provides category management features like category creation, category posting settings (the so-called "locking" of category/topic), user access control and reply voting. The system enables efficient communication and interaction within the forum while ensuring appropriate access controls and administrative functionality.

## Infrastructure, design and usage

- Relational Database (MariaDB), hosted by AWS (RDS)
- Web Framework - FastAPI
- Architectural style - REST
- Communication Protocol - HTTP
- Web server - uvicorn
- Using the API - Postman / FastAPI Swagger

***
## Setup and Run project Instructions

### 1. Setting-up virtualenv (optional)

We recommend setting up a virtual environment (venv) before installing the project dependencies. This allows you to keep the dependencies isolated from your machine's global environment. To create a virtual environment, you can use tools like Python's venv module. Once the virtual environment is activated, you can install the project dependencies without affecting your system's libraries. This ensures a clean and controlled environment for running the API.

#### 1.1. Installing virtual environment

So the terminal command for creating new venv with name 'venv' is as follows: `py -3 -m venv venv` 

#### 1.2. Choosing the venv interpreter

Further, we should specify the venv interpreter to be used within the environment. This should be the python interpretator located in the venv folder (relative path in the project workspace: `.\venv\Scripts\python.exe`). 

More details could be found here:

- [VS Code venv instructions](https://code.visualstudio.com/docs/python/environments)

- [PyCharm venv instructions](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#env-requirements)

#### 1.3. Activate the venv

The next step is to activate the venv that we just created to ensure that the dependencies are installed within the isolated environment. To activate the venv, use the appropriate command based on your operating system:

For Windows: ```venv\Scripts\activate```

For macOS/Linux: ```source venv/bin/activate```

Once the venv is activated, you can proceed with installing the project dependencies using the command provided in the installation instructions.

### 2. Install project dependencies

You will need to install all dependencies that are necessary to run the app on your local machine. 
The requirements.txt file contain all the dependencies required.

To install the virtualenv packages on the venv please run: ```pip install -r requirements.txt```

### 3. Database server

The database server is hosted by AWS (used service: RDS) and it's constatly running.
Thus, no actions needed on your side to run the server. 

### 4. Run the web server

In order to run the uvicorn web server, execute the following command on the terminal:

```python -m uvicorn main:app --reload```

Once the command is executed, the uvicorn server will start, and you should see output indicating the server's status and any logged information or errors. The API will then be accessible at the default host and port (e.g., http://127.0.0.1:8000).

Remember to keep the venv activated while the server is running to ensure that it uses the correct dependencies and environment settings.

## FastAPI automatic documentation (Swagger)

FastAPI provides automatic documentation generation, making it easier to present and communicate the API functionality. 
The documentation could be accessed while the web server is running to the default URL: http://127.0.0.1:8000/docs . 

In our project we enhanced the automatic documentation by adding a markdown docstrings to the routers which provides better user experience:

![Alt text](images/markdown.PNG)

## Database 

The MariaDB table schema is structured as follows:

![Alt text](images/diagram.PNG)

## Functionality

### Main functionality
Overall, the Forum API allows web clients to read and create topics / replies, message other users, and administrators to manage users, topics, and categories.

The app offers a range of functionalities for both users and administrators. Below we provide a short summary of the main features.

<mark>The repository contain a PostMan collection of HTTP requests which could be used for endpoints usage/testing.<mark/>

#### User Management
Users can create an account as regular users. Only existing administrators can change the role of regular user to admin user. 

#### User authentication
User authentication is implemented, allowing users to log in securely. We provide more details below.

#### Messaging System
Users can send messages to other users within the forum.
Messages can be deleted or modified as needed.
Users have access to a comprehensive list of all users with whom they have exchanged messages.

#### Forum Interaction
- Users can create new topics to initiate discussions in the respective categories.
- Replies can be posted to existing topics to contribute to the conversation.
- Users have the ability to upvote or downvote replies based on their preferences.
- The topic author can choose the best reply for a particular topic.
- Most of the enpoints have options for filtering / sorting / pagination (custom-made). 
- Users can view all public resourses, i.e., categories / topics / replies.
- Users can view or post replies / topics in private categories, depending on if the relevant user is category member having 'full' or 'read-only' access. User will have no read or write rights in case of 'restricted' access.

#### Administrator Privileges
Admins have additional privileges to manage users, categories, topics, and replies. They can control access to specific categories, topics, and replies, including locking or unlocking them. This means that:
- Admin can make a Category Private, i.e., its resourses could not be accessed by non-members. 
- Admin can add User to Category as member by granting three access levels to a User for a Category ('restricted' / 'read_only' / 'full').
- Admin can Lock/Unlock Topics/Categories.

Additionally, only Admin user can create another Admin user.

### Security / Authentication

#### 1. Security

During user registration, the app does not store plain text passwords in the database. The passwords are hashed for security purposes before being stored.

**NB!** <mark>App-sensitive data, such as the DB password, security key, etc., is stored in the .env file. Since this is a student project without a proper frontend implemented, we intentionally include the .env file in the remote repository. This allows for proper testing of the Forum API.<mark>

#### 2. Authentication

User authentication is implemented using OAuth 2, an industry-standard protocol for authorization, ensuring secure information exchange. This maintains the requirement of a stateless REST client-server communication, where the token is stored by the web client.

Upon each login, a JSON Web Token (JWT) is generated. For each subsequent user request, the token should be included in the request header. Our API then verifies the token's validity using a security algorithm and secret key. If the token is valid, the API grants access to the requested resource.

The following picture summarizes the authentication process:

![Alt text](images/oauth2.png)

**NB!** <mark>If an unauthenticated user attempts to access an endpoint that requires authentication, our API will raise a 401 HTTP Exception (Unauthorized). In such case, the web server could be restarted by pressing CTRL+S.<mark>

## Unit tests

We have diligently implemented over 140 comprehensive unit tests for both routers and services within the project. These tests   verify the functionality, behavior, and integrity of the implemented code, ensuring robustness and reliability throughout the application. By extensively testing the routers and services, we aim to deliver a highly stable and dependable system.

### Shutting down the web server

When funished using the app, web server could be shutted down by pressing `CTRL+C` in the terminal.
